var expect = require('chai').expect;
var assert = require('assert');
var request = require('supertest');
var request_session = require('supertest-session');

var async = require('async');

var mongodb = require('mongodb');
var mongo_client = mongodb.MongoClient;

var db;


function drop_test_users(done) {
    db.collection('users').drop(function (err, response) {
       done();
    });
}

process.env.env = 'test';
var app;
var agent;
var session_agent;




describe('passport', function () {


    before('passport: connect to db', function (done) {
        console.log('SIGNUP_LOGIN_TEST: BEFORE STARTED');
        mongo_client.connect('mongodb://localhost:27017/cjk_reader_test', function (err, database) {
            if (err == null) {
                db = database;
                done();
            } else {
                console.log("error: " + err);
            }
        });
    });

    beforeEach('reset app', function (done) {
        console.log('SIGNUP_LOGIN_TEST: BEFORE_EACH STARTED');
        app = require('../../../app');
        agent = request(app);
        session_agent = request_session(app);
        drop_test_users(done);
    });

    after('passport: close', function (done) {
        console.log('SIGNUP_LOGIN_TEST: AFTER STARTED');
        db.close(function (err) {
            done();
        });
    });


    describe('sessions', function () {

        it('should set session cookie on first load, and not the following', function (done) {
            agent.get('/').end(function (err, res) {
                assert(err == null);
                if (err) console.log(err);
                var session_cookie_set = false;
                for (var i = 0; i < res.res.headers['set-cookie'].length; i++) {
                    if (res.res.headers['set-cookie'][0].indexOf('connect.sid') > -1) {
                        session_cookie_set = true;
                    }
                }
                assert(session_cookie_set == true);
                done();
            });

            agent.get('/').end(function (err, res) {
                assert(err == null);
                if (err) console.log(err);
                var session_cookie_set = false;
                for (var i = 0; i < res.res.headers['set-cookie'].length; i++) {
                    if (res.res.headers['set-cookie'][0].indexOf('connect.sid') > -1) {
                        session_cookie_set = true;
                    }
                }
                assert(session_cookie_set == false);
                done();
            });
        });
    });

    describe('passport.js users', function () {

        var new_account = {
            username: 'david',
            password: 'password'
        };

        it('should save a new user', function (done) {
            agent.post('/user/signup').send(new_account).end(function (err, res) {
                assert(err == null);
                assert(res.status == 200);
                //console.log(res);
                done();
            });

        });

        it('should prevent duplicate signups', function (done) {

            async.waterfall([
                function (callback) {
                    agent.post('/user/signup').send(new_account).end(function (err, res) {
                        if (res.status != 200) console.log(res.status);
                        assert(res.status == 200);
                        callback(err);
                    });
                },
                function (callback) {
                    agent.post('/user/signup').send(new_account).end(function (err, res) {
                        if (res.status != 401) console.log(res.status);
                        assert(res.status == 401);
                        callback(err);
                    });
                }
            ], function (err, results) {
                if (err != null) console.log(err);
                assert(err == null);
                done();
            });

        });

        it('should allow users to login', function (done) {

            async.waterfall([
                function (callback) {
                    session_agent.post('/user/signup').send(new_account).end(function (err, res) {
                        if (res.status != 200) console.log(res.status);
                        //console.log(JSON.stringify(res));
                        assert(res.status == 200);
                        callback(err);
                    });
                },
                function (callback) {
                    session_agent.post('/user/login').send(new_account).end(function (err, res) {
                        if (res.status != 200) console.log(res.status);
                        //console.log(JSON.stringify(res));
                        assert(res.status == 200);
                        callback(err);
                    });
                },
                function (callback) {
                    session_agent.get('/user/file/file_list').end(function (err, res) {
                        if (res.status != 200) console.log(res.status);
                        //console.log(JSON.stringify(res));
                        assert(res.status == 200);
                        callback(err);
                    });
                }
            ], function (err, results) {
                if (err != null) console.log(err);
                assert(err == null);
                done();
            });

        });

    });

});

