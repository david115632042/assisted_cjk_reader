var spawn = require('child_process').spawn;
var path = require('path');
var net = require('net');

export class PdfboxService {

    private max_connections: number;
    private open_connections: number;
    private request_queue;

    private port;

    constructor() {
        this.max_connections = 2;
        this.open_connections = 0;
        this.request_queue = [];
    }

    public connect(port, hostName, dbName) {
        this.port = port;
        var pdfbox_path = process.env.PDFBOX_PATH;
        console.log(path.resolve(pdfbox_path));
        var pdfbox = spawn('java', ['-jar', pdfbox_path, port, hostName, dbName], {
            detatched: true
        });

        pdfbox.stdout.on('data', function (data) {
            console.log(JSON.stringify(data));
            var data_str = data.toString('UTF-8');
            console.log(data_str);
        });

        pdfbox.stderr.on('data', function (data) {
            console.log(JSON.stringify(data));
            var data_str = data.toString('UTF-8');
            console.log(data_str);
        });

    }

    public extract_pdf_text(user_id, file_id, start, end, done) {
        var client = new net.Socket();

        var command = ['extract', '--user_id', user_id, '--file_id', file_id, '--pages', start, end].join(' ');

        if (this.open_connections < this.max_connections) {
            this.open_connections++;

            var result = { res: null };
            var result_buffer = "";

            client.connect(this.port, '127.0.0.1', function (client, this_ref) {
                return function () {
                    console.log('Connected');
                    //this_ref.open_connections++;
                    client.write(command + '\n');
                };
            } (client, this));

            client.on('data', function (client, result) {
                return function (data) {
                    console.log("ON_DATA HIT" + Date());

                    if (result_buffer == "") {
                        console.log('ONDATA: empty buffer');
                        // check if json object is complete
                        var data_obj = null;
                        try {
                            data_obj = JSON.parse(data);
                        } catch (err) {
                            // if not, add to buffer and keep waiting for more data
                            result_buffer += data;
                            return;
                        }

                    } else {
                        console.log('ONDATA: buffer not empty');
                        // concat new data
                        result_buffer += data;
                        var data_obj = null;
                        try {
                            data_obj = JSON.parse(result_buffer);
                        } catch (err) {
                            // if not, keep waiting for more data
                            return;
                        }

                    }

                    // if JSON object is complete
                    var base64_response = data_obj.response.text;
                    var buf = new Buffer(base64_response, 'base64');
                    var utf_8_str = buf.toString('UTF-8');
                    data_obj.response.text = utf_8_str;
                    result.res = data_obj.response;
                    client.destroy();

                };
            } (client, result));

            client.on('error', function (error) {
                console.log('ON_ERROR');
                console.log(error);
            })

            client.on('close', function (this_ref, result) {
                return function () {
                    console.log('ON_CLOSE');
                    this_ref.open_connections--;
                    done(result.res);
                    if (this_ref.request_queue.length > 0) {
                        var next_command = this_ref.request_queue.shift();
                        this_ref.extract_pdf_text(next_command.user_id, next_command.file_id, next_command.start, next_command.end, next_command.callback);
                    }
                }
            } (this, result));
        } else {
            this.request_queue.push({
                user_id: user_id,
                file_id: file_id,
                start: start,
                end: end,
                callback: done
            });
        }

    }

}
