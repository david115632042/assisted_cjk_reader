var expect = require('chai').expect;
var assert = require('assert');
var request = require('supertest');

process.env.env = 'test';

var app;
var agent;
var session_agent;




describe('route: dictionary', function() {

    beforeEach('reset app', function (done) {
        console.log('dictionary_test: BEFOREEACH STARTED');
        app = require('../../../app');
        agent = request(app);
        done();
    });

    it('should respond with rendered template to a basic query', function(done) {

        agent.get('/api/dictionary').query({word: '私'}).end(function(err, res) {
            if (err) console.log(err);
            if (res.status != null) console.log(res.status);
            assert(err == null);
            assert(res.status == 200);
            done();
        });

    });

    it('should respond with a 404 to an unmatched query', function(done) {

        agent.get('/api/dictionary').query({word: ''}).end(function(err, res) {
            assert(err == null);
            assert(res.status == 404);
            done();
        });

    });

})