#!/usr/bin/env python
# -*- coding: utf-8 -*-

import xmltodict
import codecs
from pymongo import MongoClient
import pymongo.errors
import sys


def parent_members_dict_to_array (parent, target_members):
    for member in target_members:
        if member in parent:
            if isinstance(parent[member], dict):
                parent[member] = [parent[member]]
            elif isinstance(parent[member], str):
                parent[member] = [parent[member]]
    return parent


def handle_entry(path, item):
    try:
        global collection
        print (item)

        # <!ELEMENT entry (ent_seq, k_ele*, r_ele+, sense+)>
        item = parent_members_dict_to_array(item, ["k_ele", "r_ele", "sense"])


        # <!ELEMENT k_ele (keb, ke_inf*, ke_pri*)>
        if "k_ele" in item:
            for elem in item["k_ele"]:
                parent_members_dict_to_array(elem, ["ke_inf", "ke_pri"])


        # <!ELEMENT r_ele (reb, re_nokanji?, re_restr*, re_inf*, re_pri*)>
        if "r_ele" in item:
            for elem in item["r_ele"]:
                parent_members_dict_to_array(elem, ["re_restr", "re_inf", "re_pri"])


        # <!ELEMENT sense (stagk*, stagr*, pos*, xref*, ant*, field*, misc*, s_inf*, lsource*, dial*, gloss*)>
        if "sense" in item:
            for elem in item["sense"]:
                parent_members_dict_to_array(elem, ["stagk", "stagr", "pos", "xref", "ant", "field", "misc", "s_inf", "lsource", "dial", "gloss"])


        collection.insert_one(item)
        return True
    except pymongo.errors.DuplicateKeyError:
        print("duplicate key, skipping")
        return False


def parse_jmdict():
    with codecs.open('JMdict.xml', 'r', 'utf-8') as input_file:
        input = input_file.read()
        xmltodict.parse(input,  encoding='utf-8', item_depth=2, item_callback=handle_entry)


def get_db():
    client = MongoClient('localhost:27017')
    db = client.cjk_reader
    return db


def get_test_db():
    client = MongoClient('localhost:27017')
    db = client.test
    return db


if __name__ == "__main__":
    print(sys.version)
    db = get_db()
    global collection
    collection = db.jmdict
    parse_jmdict()
