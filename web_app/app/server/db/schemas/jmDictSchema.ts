import * as mongoose from "mongoose";

//export interface JmDictDoc extends dbType.JmDict, mongoose.Document {}

export var jmDictSchema = new mongoose.Schema({
    ent_seq: String,
    
    k_ele: [{
        keb: String, 
        ke_inf: [String], 
        ke_pri: [String]
    }],

    r_ele: [{
        reb: String, 
        re_nokanji: String, 
        re_restr: [String], 
        re_inf: [String], 
        re_pri: [String]
    }],

    sense: [{
        stagk: [String], 
        stagr: [String], 
        pos: [String], 
        xref: [String], 
        ant: [String], 
        field: [String], 
        misc: [String], 
        s_inf: [String], 
        lsource: [String], 
        dial: [String], 
        gloss: [{
            "@xml:lang": String,
            "#text": String
        }]
    }]
}, 
{
    collection: "jmdict"
});