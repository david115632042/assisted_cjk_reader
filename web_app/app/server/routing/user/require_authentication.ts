import * as express from "express";

export default function (req: express.Request, res: express.Response, next: express.NextFunction) {

    if ( !req.isAuthenticated() ) { 
        res.status( 401 );      // constant defined elsewhere, accessible here
        return res.end('Please Login'); // or a redirect in a traditional web app, 
    }                                   // as opposed to an SPA
    next();

}