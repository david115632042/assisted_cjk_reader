import java.io.File;

public interface Upload_db {

    public void connect(String hostName, String dbName, String collectionName);

    public File get_file(String user_id, String file_id);

}
