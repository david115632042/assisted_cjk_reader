
import {Passport} from "passport";
import * as passport_local from "passport-local";
var LocalStrategy = passport_local.Strategy;

import { Database } from "Server";

export var configure_passport = function(passport: Passport, db: Database) {

    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        db.get_user_by_id(id, function(err, user) {
            done(err, user);
        });
    });

    passport.use('local-signup', new LocalStrategy({
        usernameField : 'username',
        passwordField : 'password',
        passReqToCallback : true 
    },

    function(req, username, password, done) {

        // asynchronous
        // User.findOne wont fire unless data is sent back
        process.nextTick(function() {

        // find a user whose email is the same as the forms email
        // we are checking to see if the user trying to login already exists
        req.db.get_user_by_username(username, function(err, user) {
            // if there are any errors, return the error
            if (err) {
                return done('error', null, err);
            }

            // check to see if theres already a user with that email
            if (user) {
                return done(null, false);
            } else {

                // if there is no user with that email
                // create the user
                req.db.create_user(username, password, (err, new_user) => {
                    done(err, new_user);
                });
            }

        });    

        });

    }));

    passport.use('local-login', new LocalStrategy({
        usernameField : 'username',
        passwordField : 'password',
        passReqToCallback : true 
    },
    function(req, username, password, done) { // callback with email and password from our form

        // find a user whose email is the same as the forms email
        // we are checking to see if the user trying to login already exists
        req.db.get_user_by_username(username, (err, user) => {
            // if there are any errors, return the error before anything else
            if (err) {
                return done('error', null, err);
            }
                

            // if no user is found, return the message
            if (!user) {
                return done(null, false); // req.flash is the way to set flashdata using connect-flash
            }
                

            // if the user is found but the password is wrong
            if (!user.validPassword(password)) {
                return done(null, false); // create the loginMessage and save it to session as flashdata
            }
                

            // all is well, return successful user
            return done(null, user);
        });

    }));

};