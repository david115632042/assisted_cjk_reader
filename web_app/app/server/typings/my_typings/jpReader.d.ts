
declare namespace Express {
    export interface Request {
        db?: any;
        mecab_service?: any;
        pdfbox_service?: any;
    }
}


declare interface Error {
    status?: number;
}
