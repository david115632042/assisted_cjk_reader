import * as express from "express";
var router = express.Router();

export function get_routing (passport) {

    router.post('/', passport.authenticate('local-login'), function (req, res, next) {
        
        if (req.user) {
            res.json({
                error: null,
                message: 'signin succeeded'
            });
        } else {
            res.json({
                error: 'signin failed',
                message: 'signin failed'
            });

        }
        
    });

    return router;

}