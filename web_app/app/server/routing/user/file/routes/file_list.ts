import * as express from "express";
var router = express.Router();
var fs = require('fs');

import require_auth from "../../require_authentication";

export function get_routing() {

    router.get('/', require_auth, function (req, res, next) {

        req.db.get_user_file_list(req.user._id.toString(), function (err, user_uploads) {
            
            if (err) {
                res.send("an error occurred");
            }
            else {
                res.json(user_uploads);
            }
            
        });
    });

    return router;

}


