
var nodeExternals = require('webpack-node-externals');
var path = require('path');

module.exports = {
    context: __dirname,
    target: 'node',
    externals: [nodeExternals()],
    entry: 'mocha!./test/index.js',
    output: {
        filename: 'test.build.js',
        path: path.resolve(__dirname, "test"),
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                loaders: ['babel-loader']
            },
            {
                test: /(\.css|\.less)$/,
                loader: 'null-loader'
            },
            { 
                test: /\.ts$/, 
                loader: 'ts-loader' 
            }
        ]
    }
};



