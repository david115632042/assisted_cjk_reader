

declare module "Client" {

    import { Mecab } from "Api";

    interface FileListResponse {
        file_list: Mecab.MecabToken[];
    }

    interface ServerData {
        query_mecab(japanese_text, format): ng.IHttpPromise<{}>;
        query_dictionary(text, in_json: Boolean): ng.IHttpPromise<{}>;
        get_fileList(): ng.IHttpPromise<FileListResponse>;
        signup(username, password): ng.IHttpPromise<{}>;
        login(username, password): ng.IHttpPromise<{}>;
        logout(): ng.IHttpPromise<{}>;
        load_page(file_id, page): ng.IHttpPromise<{}>;
        delete_file(file_id): ng.IHttpPromise<{}>;
        upload_file(data): ng.IHttpPromise<{}>;
    }

    interface DisplayState {
        sidebar_always_visible: Boolean;
        sidebar_visible: Boolean;
        logged_in: Boolean;
        username: String;

        alert_logged_in(username: String): void;
        alert_logged_out(): void;

        alert_sidebar_visible(): void;
        alert_sidebar_hidden(): void;
    }

}