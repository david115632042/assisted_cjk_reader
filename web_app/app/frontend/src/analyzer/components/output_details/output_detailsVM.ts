
import {Mecab} from "Api";


export interface IDictionaryEntry {
    kanji: string[],
    readings: string[],
    definitions: string[]
}


export default class OutputDetailsVM {

    public dictionaryEntries: [IDictionaryEntry];


    public OutputDetailsVM(tokens: Mecab.MecabToken[]) {
        
    }

}