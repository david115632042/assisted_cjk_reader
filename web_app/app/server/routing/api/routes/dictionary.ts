import * as express from "express";
var router = express.Router();

export function get_routing() {

    router.get('/', function (req, res, next) {

        var word = req.query.word;
        var in_json = req.query.in_json;

        console.log("word is: " + word);

        req.db.get_jmdict_entry(word, function (err, entries) {
            if (err) {
                res.status(500).send("ERROR: mongodb: " + JSON.stringify(err));
                console.log("ERROR: mongodb: " + JSON.stringify(err));
            }

            console.log("found " + entries.length + " docs");
            if (entries.length == 0) res.status(404);

            var formatted = format_dictionary_response(entries, word);

            formatted.sort(function (a, b) {
                return b.priority - a.priority;
            });

            if (in_json) {
                res.json(formatted);
            } else {
                res.render('popover', { entry_list: formatted });
            }


            if (process.env.env == 'development') console.log("response: " + JSON.stringify(entries));

        });
    });

    return router;

}


var priority_regex = /nf[0-9]{2}/g;

function get_priority(str) {

    if (str === 'news1') {
        return 20;
    }
    else if (str === 'news2') {
        return 10;
    }
    else if (str === 'ichi1') {
        return 20;
    }
    else if (str === 'ichi2') {
        return 10;
    }
    else if (str === 'gai1') {
        return 10;
    }
    else if (str === 'gai2') {
        return 5;
    }
    else if (str === 'spec1') {
        return 48;
    }
    else if (str === 'spec2') {
        return 25;
    }
    else if (priority_regex.test(str)) {
        var num = str.substring(2);
        return 50 - Number(num);
    }
    else {
        return 0;
    }

}


function format_dictionary_response(results, search_word) {

    for (var i = 0; i < results.length; i++) {

        var result = results[i];
        result.priority = 0;

        for (var r_ele in result['r_ele']) {

            for (var re_pri in r_ele['re_pri']) {
                result.priority += get_priority(re_pri);
            }

            if (process.env.env == 'development') console.log("index: " + i + " priority: " + result.priority);

        }

    }

    return results;

}