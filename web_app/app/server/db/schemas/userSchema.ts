import * as mongoose from "mongoose";

var bcrypt = require('bcrypt-nodejs');

//export interface UserDoc extends dbType.User, mongoose.Document {}

export var userSchema = new mongoose.Schema({
    local: {
        username: String,
        password: String,
    },
    facebook: {
        id: String,
        token: String,
        email: String,
        name: String
    },
    twitter: {
        id: String,
        token: String,
        displayName: String,
        username: String
    },
    google: {
        id: String,
        token: String,
        email: String,
        name: String
    },
    files: [{ type: mongoose.Schema.Types.ObjectId, ref: "GridFile" }]
});

// generating a hash
userSchema.methods.generateHash = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function (password) {
    return bcrypt.compareSync(password, this.local.password);
};