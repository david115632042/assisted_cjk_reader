
import my_sidebar from "../../src/my_sidebar";

export default function (app) {

    app.directive("mySidebar", my_sidebar);

}