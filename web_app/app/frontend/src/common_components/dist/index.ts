
import material_loader from "../src/material_loader/loader";

export default function (app) {

    app.directive("materialLoader", material_loader);

}