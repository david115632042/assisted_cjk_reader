
export default function ($scope, $rootScope, $state, server_data, display_state) {
    var vm = this;

    vm.submission_in_progress = false;
    vm.clear_file_selector = clear_file_selector;
    vm.submit_file = submit_file;
    vm.on_load_file = on_load_file;
    vm.on_delete_file = on_delete_file;

    var file_input_elem: any = $("#file-input");

    file_input_elem.bind("change", function (changeEvent) {
        $scope.$apply(function () {
            vm.file = (file_input_elem[0].files[0]);
            console.log("FILE_INPUT: " + JSON.stringify(file_input_elem[0].files));
            console.log("FILE: " + JSON.stringify(vm.file));
            vm.file_input = {
                name: vm.file.name,
                type: vm.file.name.substring(vm.file.name.length - 4),
                size: vm.file.size
            };
        });
    });

    function clear_file_selector () {
        file_input_elem.val("");
        vm.file = null;
        vm.file_input = null;
    }

    function submit_file () {
        if (!vm.file_input || !vm.file) {
            toastr.error("please select a file first");
            return;
        }

        var data = {
            "filename": vm.file_input.name,
            "filetype": vm.file_input.type,
            "filesize": vm.file_input.size,
            "file_base64": null
        };

        console.log(data);
        vm.submission_in_progress = true;
        var reader = new FileReader();
        console.log("onload trigger set");
        reader.onload = function (e: any) {     // typescript thinks event object doesn't have a result attribute
            data.file_base64 = e.target.result.split(/,/)[1];
            console.log("uploading file");

            server_data.upload_file(data).then(function success(response) {
                vm.submission_in_progress = false;
                toastr.success("file submitted successfully");
                $rootScope.$broadcast("file_list_updated", "new file submitted");
                vm.clear_file_selector();
            }, function error(response) {
                vm.submission_in_progress = false;
                toastr.error("error submitting file");
            });

        };
        reader.readAsDataURL(vm.file);
    }

    function on_load_file (file_id: String) {
        $state.go("analyzer_output.file", { input_file_id: file_id, page: 0 });
    }

    function on_delete_file(file_id: String) {
        server_data.delete_file(file_id).then(function success(response) {
            toastr.success("successfully deleted file");
            $rootScope.$broadcast("file_list_updated", "file deleted");
        }, function error(response) {
            if (response.status === 401) {
                display_state.alert_logged_out();
            } else {
                toastr.error("error deleting file");
            }
            $rootScope.$broadcast("file_list_updated", "file deleted");
        });
    }

};