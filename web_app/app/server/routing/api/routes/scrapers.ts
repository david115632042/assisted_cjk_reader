import * as express from "express";
var router = express.Router();

var request = require('request');
var cheerio = require('cheerio');

var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');

export function get_routing (): express.Router {

    router.get('/', function (req, res, next) {

        var url = req.param('url');
        console.log('url is: ' + url);

        if (url == "asahi") {
            url = "http://www.asahi.com/news/?iref=comtop_gnavi";
        }

        request(url, function (error, response, html) {

            if (!error) {

                var $ = cheerio.load(html);
                //res.send($.filter('#Headlines-List').html());

                var links = [];
                $('#Headlines-List').children('.List').children('li').each(function (i, elem) {

                    var link = $(this);
                    links[i] = { link_title: link.children().text(), link_url: "http://www.asahi.com" + link.children().attr('href') };


                });

                res.json(links);
                save_articles(links);
                console.log("This got hit after the response");

            } else {
                res.send(error);
                //return 'error while scraping website';
            }
        });

        //res.send(scrape_website(url));
    });

    return router;

}

function save_articles(links) {

    // Connection URL
    var address = process.env.MONGO_ADDRESS;
    var port = process.env.MONGO_PORT;
    var mongo_url = "mongodb://" + address + ":" + port + "/blog";
    //var mongo_url = "mongodb://"+"172.17.0.1"+":"+"27017"+"/blog";
    console.log("mongo_url is: " + mongo_url);
    // Use connect method to connect to the Server
    MongoClient.connect(mongo_url, function (err, db) {
        assert.equal(null, err);
        console.log("Connected correctly to mongoDB server");

        db.close();
    });

    for (var i = 0; i < links.length; i++) {
        var title = links[i].link_title;
        var url = links[i].link_url;
        if (title != "" && url != "") {

        }
    }
}


function scrape_website(url) {

    request(url, function (error, response, html) {

        if (!error) {

            return html;
            //var $ = cheerio.load(html);
            //return JSON.stringify($);
        } else {
            return 'error while scraping website';
        }
    });
}


function insert_document(db, document, callback) {
    // Get the documents collection
    var collection = db.collection('online_resources');
    // Insert some documents

    if (collection.find({ "document_url": document["url"] }).count() == 0) {
        // document does not currently exist in db
        collection.insertOne(document);

    } else {
        console.log("document " + document["url"] + " already exists in db");
    }

    collection.insertMany([
        { a: 1 }, { a: 2 }, { a: 3 }
    ], function (err, result) {
        assert.equal(err, null);
        assert.equal(3, result.result.n);
        assert.equal(3, result.ops.length);
        console.log("Inserted 3 documents into the document collection");
        callback(result);
    });
}
