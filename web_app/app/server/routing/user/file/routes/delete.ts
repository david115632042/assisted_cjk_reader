import * as express from "express";
var router = express.Router();

import require_auth from "../../require_authentication";

export function get_routing() {

    router.post('/', require_auth, function (req, res, next) {

        var file_id = req.query.file_id;
        var user_id = req.user._id;

        req.db.delete_upload(user_id, file_id, function (err, updated_file_list) {
            if (err) {
                res.status(500);
                res.send("error deleting file");
            } else {
                res.send("successfully deleted file");
            }
        });

    });

    return router;

}