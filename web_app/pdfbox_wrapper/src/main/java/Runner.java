import java.io.IOException;
import java.net.ServerSocket;

public class Runner {

    private Upload_db db;
    private int port;

    public Runner(Upload_db _db_, int _port_) {
        this.db = _db_;
        this.port = _port_;
    }

    public void run() {

        ServerSocket server = null;

        try {
            server = new ServerSocket(port, 128);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        } finally {
            if (server == null) {
                System.out.println("server socket could not be created");
                System.exit(1);
            }
        }

        while (true) {

            try {
                new Client_handler(server.accept(), db).start();
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }

        }

    }

}
