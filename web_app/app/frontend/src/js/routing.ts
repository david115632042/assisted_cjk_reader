


export default function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("/welcome");

    $stateProvider
        .state("welcome", welcome)

        .state("analyzer_input", analyzer_input)
        .state("analyzer_input.text", analyzer_input_text)
        .state("analyzer_input.file", analyzer_input_file)
        .state("analyzer_input.online_resource", analyzer_input_online_resource)

        .state("analyzer_output", analyzer_output)
        .state("analyzer_output.text", analyzer_output_text)
        .state("analyzer_output.file", analyzer_output_file)
        .state("analyzer_output.online_resource", analyzer_output_online_resource)

        .state("profile", profile)
        .state("about", about)
        .state("settings", settings);

};


var welcome = {
    url: "/welcome",
    views: {
        "main": {
            templateUrl: "templates/welcome.html",
            controller: "welcome_controller",
            controllerAs: "vm"
        }
    }

};

var analyzer_input = {
    url: "/analyzer_input",
    views: {
        "main": {
            templateUrl: "templates/analyzer/analyzer_input.html",
            controller: "analyzer_input_controller",
            controllerAs: "vm"
        }
    }

};

var analyzer_input_text = {
    url: "/text",
    parent: "analyzer_input",
    views: {
        "main": {
            templateUrl: "templates/analyzer/analyzer_input_text.html",
            controller: "analyzer_input_text_controller",
            controllerAs: "vm"
        }
    }
};

var analyzer_input_file = {
    url: "/file",
    parent: "analyzer_input",
    views: {
        "main": {
            templateUrl: "templates/analyzer/analyzer_input_file.html",
            controller: "analyzer_input_file_controller",
            controllerAs: "vm"
        }
    }
};

var analyzer_input_online_resource = {
    url: "/online_resource",
    parent: "analyzer_input",
    views: {
        "main": {
            templateUrl: "templates/analyzer/analyzer_input_online_resource.html",
            controller: "analyzer_input_online_resource_controller",
            controllerAs: "vm"
        }
    }
};

var analyzer_output = {
    url: "/analyzer_output",
    abstract: true,
    views: {
        "main": {
            template: `<div ui-view="main" class="analyzer-output-wrapper"></div>`
        }
    }
    
};

var analyzer_output_text = {
    url: "/text?input_text",
    parent: "analyzer_output",
    views: {
        "main": {
            templateUrl: "templates/analyzer/analyzer_output_text.html",
            controller: "analyzer_output_text_controller",
            controllerAs: "vm"
        }
    },
    params: {
        input_text: null
    }

};

var analyzer_output_file = {
    url: "/file?input_file_id?page",
    parent: "analyzer_output",
    views: {
        "main": {
            templateUrl: "templates/analyzer/analyzer_output_file.html",
            controller: "analyzer_output_file_controller",
            controllerAs: "vm"
        }
    },
    params: {
        input_file_id: null,
        page: "0"
    }
};

var analyzer_output_online_resource = {
    url: "/online_resource?article_id",
    parent: "analyzer_output",
    views: {
        "main": {
            templateUrl: "templates/analyzer/analyzer_output_online_resource.html",
            controller: "analyzer_output_online_resource_controller",
            controllerAs: "vm"
        }
    },
    params: {
        article_id: null
    }
};

var profile = {
    url: "/profile",
    views: {
        "main": {
            templateUrl: "templates/profile.html"
        }
    }
};

var about = {
    url: "/about",
    views: {
        "main": {
            templateUrl: "templates/about.html"
        }
    }
};

var settings = {
    url: "/settings",
    views: {
        "main": {
            templateUrl: "templates/settings.html"
        }
    }
};