#!/bin/bash

apt update;
apt upgrade -y;

apt install -y nodejs npm;
ln -s /usr/bin/nodejs /usr/bin/node;
npm install -g gulp;

wget wget https://www.python.org/ftp/python/2.7.11/Python-2.7.11.tgz;
tar zxf Python-2.7.11.tgz;
cd Python-2.7.11;
./configure;
make install;

apt install python-pip;
pip install --upgrade pip;
pip install xmltodict;
pip install pymongo;

sh docker_install.sh;

curl -L https://github.com/docker/compose/releases/download/1.7.1/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose;
chmod +x /usr/local/bin/docker-compose
