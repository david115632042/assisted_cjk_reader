
public class Main {

    public static void main(String[] args) {

        System.out.println("running");

        int portNumber = Integer.parseInt(args[0]);
        String hostName = args[1];
        String dbName = args[2];

        Upload_db db = new Mongo_upload_db();

        db.connect(hostName, dbName, "uploads");

        Runner runner = new Runner(db, portNumber);
        runner.run();

    }

}
