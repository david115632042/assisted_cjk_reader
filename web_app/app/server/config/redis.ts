

export function get_url(): string {
  if (process.env.env == 'development' || process.env.env == 'test') {
    return 'redis://localhost:6379';
  } else {
    return 'redis://redis:6379';
  }
}
