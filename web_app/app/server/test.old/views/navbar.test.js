var expect = require('chai').expect;
var assert = require('assert');
var request = require('supertest');
var request_session = require('supertest-session');

var async = require('async');

var mongo_client = require('mongodb').MongoClient;

var db;


function drop_test_users(done) {
    db.collection('users').drop(function (err, response) {
       done();
    });
}

process.env.env = 'test';
var app;
var agent;


describe('navbar', function() {

    before('navbar: connect to db', function (done) {
        console.log('navbar_test: BEFORE STARTED');
        mongo_client.connect('mongodb://localhost:27017/cjk_reader_test', function (err, database) {
            if (err == null) {
                db = database;
                done();
            } else {
                console.log("error: " + err);
            }
        });
    });

    beforeEach('reset app', function (done) {
        console.log('navbar_test: BEFORE_EACH STARTED');
        app = require('../../../app');
        agent = request(app);
        drop_test_users(done);
    });

    after('navbar: close', function (done) {
        db.close(function (err) {
            done();
        });
    });


    var new_account = {
        username: 'david',
        password: 'password'
    };

    it('should send a different template after logging in', function(done) {

        var etag = null;
        var session_cookie = null;

        async.waterfall([
            function (callback) {
                agent.get('/view/navbar').end(function (err, res) {
                    assert(res.status == 200);
                    etag = res.headers.etag;
                    callback(err);
                });
            },
            function (callback) {
                agent.post('/user/signup').send(new_account).end(function (err, res) {
                    if (res.status != 200) console.log(res.status);
                    assert(res.status == 200);
                    callback(err);
                });
            },
            function (callback) {
                agent.post('/user/login').send(new_account).end(function (err, res) {
                    if (res.status != 200) console.log(res.status);
                    session_cookie = res.headers['set-cookie'][0];
                    assert(res.status == 200);
                    callback(err);
                });
            },
            function (callback) {
                agent.get('/view/navbar').set('If-None-Match', etag).set('Cookie', session_cookie).end(function (err, res) {
                    assert(res.status == 200);
                    etag = res.headers.etag;
                    callback(err);
                });
            },
            function (callback) {
                agent.get('/view/navbar').set('If-None-Match', etag).set('Cookie', session_cookie).end(function (err, res) {
                    assert(res.status == 304);
                    etag = res.headers.etag;
                    callback(err);
                });
            }
        ], function (err, res) {
            assert(err == null);
            done();
        });

    });

});