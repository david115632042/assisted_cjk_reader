
import { DisplayState } from "Client";

export default function(display_state: DisplayState) {

    return {
        restrict: "E",
        template: require("../navbar.html"),
        link: function( $scope, elem, attrs ) {
            
            $scope.logged_in = display_state.logged_in;

            $scope.sidebar_visible = display_state.sidebar_visible;

            $scope.$watch(() => { 
                return display_state.sidebar_visible; 
            }, handle_sidebar_toggled_updated);

            $scope.show_sidebar = show_sidebar;

            $scope.$watch(function() { return display_state.logged_in; }, handle_logged_in);

            function handle_sidebar_toggled_updated (sidebar_visible) {
                $scope.sidebar_visible = sidebar_visible;
            }

            function show_sidebar () {
                display_state.alert_sidebar_visible();
                // $("#page-nav-and-content-wrapper").toggleClass("sidebar-visible");
            }

            function handle_logged_in (logged_in) {
                $scope.logged_in = logged_in;
            }

        }
    };
    
};