
import {ServerData} from "Client";

export default function ($rootScope, server_data: ServerData) {
    return {
        restrict: "E",
        scope: {},
        template: require("./file_selector.html"),
        link: function ($scope, elem, attr) {

            var file_input_elem = elem.find("#file-input");

            file_input_elem.bind("change", function (changeEvent) {
                $scope.$apply(function () {
                    $scope.file = file_input_elem[0].files[0];
                    console.log("FILE_INPUT: " + JSON.stringify(file_input_elem[0].files));
                    console.log("FILE: " + JSON.stringify($scope.file));
                    $scope.file_input = {
                        name: $scope.file.name,
                        type: $scope.file.name.substring($scope.file.name.length - 4),
                        size: $scope.file.size
                    };
                });
            });

            $scope.clear_file_selector = function () {
                file_input_elem.val("");
                $scope.file = null;
                $scope.file_input = null;
            };

            $scope.submit_file = function () {

                if (!$scope.file_input || !$scope.file) {
                    toastr.error("please select a file first");
                    return;
                }

                var data = {
                    "filename": $scope.file_input.name,
                    "filetype": $scope.file_input.type,
                    "filesize": $scope.file_input.size,
                    "file_base64": null
                };

                console.log(data);
                var reader = new FileReader();
                console.log("onload trigger set");
                reader.onload = function (e: any) {     // typescript thinks event object doesn't have a result attribute
                    data.file_base64 = e.target.result.split(/,/)[1];
                    console.log("uploading file");

                    server_data.upload_file(data).then(function success (response) {
                        console.log("success");
                        toastr.success("file submitted successfully");
                        $rootScope.$broadcast("file_list_updated", "new file submitted");
                        $scope.clear_file_selector();
                    }, function error (response) {
                        console.log("error");
                        toastr.error("error submitting file");
                    });

                };
                reader.readAsDataURL($scope.file);
            };

        }
    };
}