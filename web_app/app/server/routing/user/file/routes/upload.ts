import * as express from "express";
var router = express.Router();
var fs = require('fs');

import { dbType } from "Server";

import require_auth from "../../require_authentication";

export function get_routing () {

    router.post('/', require_auth, function (req, res, next) {

        var user_id = req.user._id.toString();
        console.log('user id: ' + user_id);

        var file = JSON.parse(req.body.file);

        var filename = file.filename;
        var filetype = file.filetype;
        var filesize = file.filesize;
        var filedata = file.file_base64;

        var pdf = filedata.replace('data:application/pdf;base64,', '');

        var new_upload: dbType.File = {
            filename: filename,
            filetype: filetype,
            //filesize: filesize,
            filedata: filedata
        }

        req.db.save_upload(user_id, new_upload, function (err, updated_file) {
            if (err) {
                res.send(err);
                return;
            }

            console.log("NEW FILE: " + updated_file);

            res.json({
                err: null,
                message: 'upload saved successfully',
                upload_id: updated_file.id
            });

        });

    });

    return router;

}