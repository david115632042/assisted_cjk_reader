
export default function($state) {
    var vm = this;

    vm.analyzer_input = "";
    vm.load_text = load_text;

    function load_text() {
        var text_in = vm.analyzer_input;

        if (text_in.length === 0) {
            toastr.error("no input detected");
        } else {
            $state.go("analyzer_output.text", { input_text: text_in });
        }
    }

};