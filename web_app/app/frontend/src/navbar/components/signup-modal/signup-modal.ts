
export default function(server_data, display_state) {

    return {
        restrict: "E",
        scope: true,
        template: require("./signup-modal.html"),
        link: function( $scope, elem, attrs ) {
            $scope.username = "";
            $scope.password = "";

            $scope.submit = submit;

            function submit () {
                server_data.signup($scope.username, $scope.password).then(function (response) {
                    console.log(JSON.stringify(response));
                    if (response.status == 200) {
                        toastr.success("Signed up successfully");
                        $("#signup-modal").modal("hide");
                    } else {
                        toastr.error(response.headers("www-authenticate"));
                    }
                });
            }
        }
    };
};