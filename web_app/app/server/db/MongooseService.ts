
import * as mongoose from "mongoose";
(<any>mongoose).Promise = require("es6-promise");
//var fs = require('fs');
import * as stream from "stream";
var Grid = require('gridfs-stream');
Grid.mongo = mongoose.mongo;


import { dbType } from "Server";

import { userSchema } from "./schemas/userSchema";
import { jmDictSchema } from "./schemas/jmDictSchema";
import { gridFileSchema } from "./schemas/gridFileSchema";


export default class MongooseService {

    private connection: mongoose.Connection;
    private gridFs;
    private user_model: mongoose.Model<dbType.UserDoc>;
    private jmDict_model: mongoose.Model<dbType.JmDictDoc>;
    private gridModel: mongoose.Model<mongoose.Document>;

    constructor() {

    }


    public connect(url, done: (err: Error) => void) {
        this.connection = mongoose.createConnection(url, (err: Error) => {
            if (err) console.log('MONGOOSE CONNECT ERROR: ' + err);
            this.gridFs = Grid(this.connection.db, mongoose.mongo);
            this.connection.model("User", userSchema);
            this.connection.model("Jmdict", jmDictSchema);
            this.connection.model("GridFile", gridFileSchema, "fs.files");
            this.user_model = this.connection.model<dbType.UserDoc>("User");
            this.jmDict_model = this.connection.model<dbType.JmDictDoc>("Jmdict");
            this.gridModel = this.connection.model<mongoose.Document>("GridFile");
            done(err);

        });
    }


    public close(done: () => void) {
        this.connection.close(() => {
            done();
        })
    }


    public save_upload(userId: string, file: dbType.File, callback: (err: string, updated_uploads: any) => void) {

        var newFileId = new mongoose.Types.ObjectId();
        var writestream = this.gridFs.createWriteStream({
            _id: newFileId,
            filename: file.filename
        });

        this.user_model.findById(userId)
            .populate("files")
            .exec((err, user) => {

                if (err) {
                    console.error(err);
                    callback(err, null);
                    return;
                }

                (<any>user).files.push(newFileId);

                user.save((err, doc, rows) => {

                    var fileStream = new stream.Readable();
                    fileStream.push(file.filedata);
                    fileStream.push(null);
                    fileStream.pipe(writestream);

                    writestream.on('close', function (file) {
                        callback(null, file);
                    });

                });

            });

    }

    
    public delete_upload(userId, file_id, callback: (err, file) => void) {
        
        var filename = "my_file.txt";
        this.gridModel.findOneAndRemove({ _id: file_id }, (err, file) => {
            console.log("FILE: " + JSON.stringify(file));
            this.user_model.findOne({ _id: userId }, (err, user) => {
                
                user.files = user.files.filter((x) => (!(<mongoose.Types.ObjectId><any>x).equals(new mongoose.Types.ObjectId(file_id))));
                user.save((err) => {
                    callback(err, null);
                });
                
            });
        });
        
    };
    

    public get_user_file_list(userId: string, callback: (err, user) => void) {
        
        this.user_model.aggregate([{
            $match: {
                "_id": new mongoose.Types.ObjectId(userId)
            }
        },
        {
            $unwind: "$files"
        },
        {
            $lookup: {
                from: "fs.files",
                localField: "files",
                foreignField: "_id",
                as: "fileList"
            }
        },
        
        {
            $unwind: "$fileList"
        },
        
        {
            $group: {
                _id: { 
                    userId: "$_id" 
                },
                fileList: { $push: "$fileList" },
                spaceUsed: {
                    $sum: "$fileList.length"
                }
            }
        }
        ], (err, users) => {
            if (users.length == 1) {
                callback(err, users[0]);
            }
            else {
                callback("did not return a single user from query", users);
            }
        });
        
    }


    public create_user(username, password, callback: (err, new_user) => void): void {
        // create the user
        var newUser = new this.user_model;

        // set the user's local credentials
        newUser.local.username = username;
        newUser.local.password = newUser.generateHash(password);

        // save the user
        newUser.save(function (err) {
            callback(err, newUser);
        });

    }


    public get_user_by_id(id, callback: (err, user: dbType.UserDoc) => void): void {
        this.user_model.findById(id, function (err, user) {
            callback(err, user);
        });
    }


    public get_user_by_username(username, callback: (err, user: dbType.UserDoc) => void): void {
        this.user_model.findOne({ 'local.username': username }, function (err, user) {
            callback(err, user);
        });
    }


    public get_jmdict_entry(word: string, callback: (err, docs) => void): void {

        this.jmDict_model.find({ $or: [{ "k_ele.keb": word }, { "r_ele.reb": word }] }, (err, res) => {
            callback(err, res);
        });

    }

}
