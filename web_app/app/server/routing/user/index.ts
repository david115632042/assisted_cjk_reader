import * as express from "express";
var users = express.Router();

import * as file_router from "./file";
import * as signup from "./routes/signup";
import * as login from "./routes/login";
import * as logout from "./routes/logout";

export function get_routing (passport) {

    users.use('/file', file_router.get_routing());
    users.use('/signup', signup.get_routing(passport));
    users.use('/login', login.get_routing(passport));
    users.use('/logout', logout.get_routing());

    return users;

}