var express = require('express');
var api = express.Router();

import * as dictionary from "./routes/dictionary";
import * as parse from "./routes/parse";
import * as scrapers from "./routes/scrapers";

export function get_routing () {
    
    api.use('/dictionary', dictionary.get_routing());
    api.use('/parse', parse.get_routing());
    api.use('/scrapers', scrapers.get_routing());

    return api;

}