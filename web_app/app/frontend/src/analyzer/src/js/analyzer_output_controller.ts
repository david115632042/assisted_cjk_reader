
import { Mecab } from "Api";

export default function ($stateParams, $rootScope, $state, server_data) {
    var vm = this;

    vm.input = {};
    
    vm.query_mecab = query_mecab;
    vm.load_page = load_page;
    vm.load_details = load_details;

    vm.pressed_last_page = pressed_last_page;
    vm.pressed_next_page = pressed_next_page;
    vm.pressed_go = pressed_go;

    vm.page_num = Number($stateParams.page);

    vm.set_word_class = set_word_class;

    if ($stateParams.input_text) {
        vm.input_type = "text";
        query_mecab($stateParams.input_text);
    } else if ($stateParams.input_file_id) {
        vm.input_type = "file";
        load_page($stateParams.input_file_id, $stateParams.page);
    }

    function query_mecab (text_in): void {
        if (text_in.length === 0) { return; }
        vm.loading = true;

        server_data.query_mecab(text_in, "pretty").then(function success (response) {
            vm.words_array = response.data;
            vm.loading = false;
        }, function error (response) {
            vm.loading = false;
        });
    }

    function load_page (file_id, page): void {
        
        vm.loading = true;

        server_data.load_page(file_id, page).then(function success (response) {

            vm.loading = false;
            vm.words_array = [];
            vm.num_of_pages = response.data.pages;

            var break_obj = function () { return { surface_form: "。  ", part_of_speech: { primary: "Particle" } }; };
            response.data.text.forEach(function (sentence) {

                sentence.forEach(function (term) {
                    vm.words_array.push(term);
                }, this);
                vm.words_array.push(break_obj());

            }, this);

        }, function error (response) {
            vm.loading = false;
            toastr.error("could not complete request");
        });

    }

    function load_details (word): void {
        var word = (word.root_form && word.root_form !== "*") ? word.root_form : word.surface_form;
        $rootScope.$broadcast("load_details", { target: word });
    }

    function go_to_page (page): void {
        $state.go("analyzer_output", { input_file_id : $stateParams.input_file_id, page: page });
    }

    function pressed_last_page (): void {
        go_to_page((Number($stateParams.page) - 1).toString());
    }

    function pressed_next_page (): void {
        go_to_page((Number($stateParams.page) + 1).toString());
    }

    function pressed_go (): void {
        go_to_page(vm.page_num);
    }

    function set_word_class (word: Mecab.PosMember): String {
        if (word === "noun") {
            return "output-word-N";
        } else if (word === "particle") {
            return "output-word-Pa";
        } /*else if (word === "symbol") {
            return "output-word-Sy";
        }*/ else if (word === "adjective") {
            return "output-word-Adj";
        } else if (word === "verb") {
            return "output-word-V";
        } else if (word === "adverb") {
            return "output-word-Adv";
        } else if (word === "auxiliary verb") {
            return "output-word-AuxV";
        }
    }

}