import { expect, assert } from "chai";
import * as path from "path";

import * as mongoose from "mongoose";
var test_mongo_url = 'mongodb://localhost:27017/test';
var connection = mongoose.createConnection(test_mongo_url);
connection.on('error', console.error.bind(console, 'connection error:'));


var fs = require('fs');
var stream = require('stream');
var Grid = require('gridfs-stream');
Grid.mongo = mongoose.mongo;

var userSchema = new mongoose.Schema({
    username: String,
    fileList: [{ type: mongoose.Schema.Types.ObjectId, ref: "GridFile" }]
});
connection.model("user", userSchema);
var gridSchema = new mongoose.Schema({}, { strict: false });
var gridModel = connection.model("GridFile", gridSchema, "fs.files");


interface UserModel extends mongoose.Document {
    username: string,
    fileList: mongoose.Types.ObjectId[]
}

var userModel = connection.model<UserModel>("user");


var gfs;
before("setup gridfs", (done) => {
    connection.once('open', function () {
        gfs = Grid(connection.db, mongoose.mongo);
        //mockgoose.reset();
        done();
    });
})


before((done) => {
    connection.db.dropDatabase().then(function success() {
        done();
    });
});


describe("gridfs", () => {

    it("should save a new user successfully", (done) => {
        userModel.create({
            username: "David"
        }, (err, doc) => {
            assert.isNull(err);
            console.log(JSON.stringify(doc));
            done();
        });
    });

    it("should be able to retrieve that user", (done) => {
        userModel.findOne({
            username: "David"
        }, (err, doc) => {
            assert.isNull(err);
            console.log(JSON.stringify(doc));
            done();
        });
    });


    it("should be able to store strings", (done) => {

        var new_fileId = new mongoose.Types.ObjectId();
        var writestream = gfs.createWriteStream({
            filename: 'PDFAsString.pdf',
            _id: new_fileId
        });

        var fileString = "this is a string i want to save";
        var s = new stream.Readable();
        s.push(fileString);
        s.push(null); // Push null to end stream
        s.pipe(writestream);


        userModel.findOne({
            username: "David"
        }, (err, doc) => {
            assert.isNull(err);
            console.log(JSON.stringify(doc));
            //assert.equal(doc.lastName, "Towson");

            doc.fileList.push(new_fileId);
            doc.save((err, doc, rows) => {

                writestream.on('close', function (file) {
                    // do something with `file` 
                    console.log("STRING: " + JSON.stringify(file));
                    //assert.equal(file.filename, "test_upload.pdf");
                    done();
                });

            });

        });

    });


    it("should be able to store simple text files", (done) => {

        // streaming to gridfs 

        var new_fileId = new mongoose.Types.ObjectId();
        var writestream = gfs.createWriteStream({
            filename: 'my_file.txt',
            _id: new_fileId
        });
        fs.createReadStream('/home/david/Documents/workspace/web/assisted_cjk_reader/web_app/app/server/test/mongoose/sample.txt').pipe(writestream);


        userModel.findOne({
            username: "David"
        }, (err, doc) => {
            assert.isNull(err);
            console.log(JSON.stringify(doc));
            //assert.equal(doc.lastName, "Towson");

            doc.fileList.push(new_fileId);
            doc.save((err, doc, rows) => {

                writestream.on('close', function (file) {
                    // do something with `file` 
                    console.log("TEXT FILE: " + JSON.stringify(file));
                    //assert.equal(file.filename, "my_file.txt");
                    done();
                });

                //done();
            });

            //done();
        });

    });


    it("should be able to store a pdf", (done) => {

        // streaming to gridfs 
        var new_fileId = new mongoose.Types.ObjectId();
        var writestream = gfs.createWriteStream({
            filename: "test_upload.pdf",
            _id: new_fileId
        });
        fs.createReadStream('/home/david/Documents/workspace/web/assisted_cjk_reader/web_app/app/server/test/test_files/test_upload.pdf').pipe(writestream);


        userModel.findOne({
            username: "David"
        }, (err, doc) => {
            assert.isNull(err);
            console.log(JSON.stringify(doc));
            //assert.equal(doc.lastName, "Towson");

            doc.fileList.push(new_fileId);
            doc.save((err, doc, rows) => {

                writestream.on('close', function (file) {
                    // do something with `file` 
                    console.log("PDF FILE: " + JSON.stringify(file));
                    //assert.equal(file.filename, "test_upload.pdf");
                    done();
                });

            });

        });

    });


    it("should be able to populate user files", (done) => {

        userModel.findOne({
            username: "David"
        })
            .populate("fileList")
            .exec((err, docs) => {
                console.log(err);
                console.log(JSON.stringify(docs));
                done();
            });

    });

    it("should return matching files with an $in query", (done) => {

        userModel.aggregate([{
            $match: {
                "username": "David"
            }
        },
        {
            $unwind: "$fileList"
        },
        {
            $lookup: {
                from: "fs.files",
                localField: "fileList",
                foreignField: "_id",
                as: "fileList"
            }
        },

        {
            $unwind: "$fileList"
        },

        {
            $group: {
                _id: {
                    userId: "$_id"
                },
                fileList: { $push: "$fileList" },
                spaceUsed: {
                    $sum: "$fileList.length"//{ $first: "$file"}
                }
            }
        }
        ], (err, files) => {
            console.log(JSON.stringify(files, null, '\t'));
            done();
        });

    });

    it("should be able to remove files from user's fileList", (done) => {

        var filename = "my_file.txt";
        gridModel.findOne({ filename: filename }, (err, file) => {
            console.log("FILE: " + JSON.stringify(file));
            var fileId = file._id;
            userModel.findOne({ username: "David" }, (err, user) => {
                console.log("USER: " + JSON.stringify(user));
                console.log("FILEID: " + fileId);
                console.log("FILEID: objectId " + JSON.stringify(new mongoose.Types.ObjectId(fileId)));
                user.fileList.forEach(elem => {
                    console.log("equality check: " + JSON.stringify(elem.equals(new mongoose.Types.ObjectId(fileId))));
                });
                user.fileList = user.fileList.filter((x) => (!x.equals(new mongoose.Types.ObjectId(fileId))));
                console.log("UPDATED USER: " + JSON.stringify(user));
                user.save((err) => {
                    gridModel.findOneAndRemove({ _id: fileId}, (err, file) => {
                        done();
                    });
                    
                });
                
            });
        });

    });

});