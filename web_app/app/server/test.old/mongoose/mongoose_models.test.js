var expect = require('chai').expect;
var assert = require('assert');

var async = require('async');
var path = require('path');

var mongoose = require('mongoose');
mongoose.Promise = require('q').Promise;
var test_mongo_url = 'mongodb://localhost:27017/cjk_reader_test';

var db_dir = '../../../server/db/';
var user_model;
var file_model;
var user_uploads_model;

function cleanup(callback) {

    async.parallel({
        user_model: function(next) {
            user_model.remove({}, function(err) {
                next();
            });
        },
        file_model: function(next) {
            file_model.remove({}, function(err) {
                next();
            });
        },
        user_uploads_model: function(next) {
            user_uploads_model.remove({}, function(err) {
                next();
            })
        }
        
    }, function(err, results) {
        callback();
    });

}

var mongoose_connection;



describe('mongoose', function () {


    before('mongoose: connect', function (done) {
        console.log('MONGOOSE_MODELS: BEFORE STARTED');
        mongoose_connection = mongoose.createConnection(test_mongo_url);
        require(path.join(db_dir, 'models', 'users'))(mongoose_connection);
        require(path.join(db_dir, 'models', 'file'))(mongoose_connection);
        require(path.join(db_dir, 'models', 'user_uploads'))(mongoose_connection);
        user_model = mongoose_connection.model('User');
        file_model = mongoose_connection.model('File');
        user_uploads_model = mongoose_connection.model('Uploads');

        done();
    });

    beforeEach('cleanup', function (done) {
        console.log('MONGOOSE_MODELS: BEFORE_EACH STARTED');
        cleanup(done);
    });

    after('mongoose: close', function (done) {
        mongoose_connection.close(function () {
            done();
        });
    });



    describe('mongoose models', function () {

        describe('file model', function () {

            it('should save new files', function (done) {
                var new_file = new file_model({ filename: 'new_file', filetype: 'new_filetype', filesize: 42, filedata: 'abcd' });

                async.waterfall([
                    function (callback) {
                        new_file.save(function (err) {
                            callback(err);
                        });
                    },
                    function (callback) {
                        file_model.findOne({ filename: 'new_file' }, function (err, file) {
                            callback(err, file);
                        });
                    }
                ], function (err, file) {
                    assert(err == null);
                    assert(file.filename == 'new_file');
                    done();
                });

            });

        });

        describe('user uploads model', function (done) {

            it('should save new uploads', function (done) {
                var new_file = new file_model({ filename: 'upload', filetype: 'filetype', filesize: 42, filedata: 'abcd' });
                var new_user_upload = new user_uploads_model({ user_id: 'my_id', file_list: [new_file] });

                async.waterfall([
                    function (callback) {
                        new_user_upload.save(function (err) {
                            callback(err);
                        });
                    },
                    function (callback) {
                        user_uploads_model.findOne({ user_id: 'my_id' }, function (err, uploads) {
                            callback(err, uploads);
                        });
                    }
                ], function (err, uploads) {
                    assert(err == null);
                    assert(uploads.file_list.length == 1);
                    assert(uploads.file_list[0].filename == 'upload');
                    done();
                });

            });

            it('should update existing uploads', function (done) {

                var new_file = new file_model({ filename: 'upload', filetype: 'filetype', filesize: 42, filedata: 'abcd' });
                var new_user_upload = new user_uploads_model({ user_id: 'my_id', file_list: [new_file] });

                async.waterfall([
                    function (callback) {
                        new_user_upload.save(function (err) {
                            callback(err);
                        });
                    },
                    function (callback) {
                        user_uploads_model.findOne({ user_id: 'my_id' }, function (err, uploads) {
                            callback(err, uploads);
                        });
                    },
                    function (uploads, callback) {
                        var another_new_file = new file_model({ filename: 'second_upload', filetype: 'filetype', filesize: 42, filedata: 'abcdefgh' });
                        uploads.file_list.push(another_new_file);
                        uploads.save(function (err) {
                            callback(err);
                        });
                    },
                    function (callback) {
                        user_uploads_model.findOne({ user_id: 'my_id' }, function (err, updated_uploads) {
                            callback(err, updated_uploads);
                        });
                    }
                ], function (err, updated_uploads) {
                    assert(err == null);
                    assert(updated_uploads.file_list.length == 2);
                    assert(updated_uploads.file_list[0].filename == 'upload');
                    assert(updated_uploads.file_list[1].filename == 'second_upload');
                    done();
                });
            });
        });
    });
});


