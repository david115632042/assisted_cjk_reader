import * as express from "express";
var navbar = express.Router();

export function get_routing () {

    navbar.get('/', function (req, res, next) {

        res.render('navbar', {
            logged_in: req.isAuthenticated()
        });

    });

    return navbar;

}

