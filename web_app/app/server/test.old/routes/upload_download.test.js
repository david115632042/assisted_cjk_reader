
var expect = require('chai').expect;
var assert = require('assert');
var request = require('supertest');

process.env.env = 'test';
process.env.PDFBOX_DIR = "/home/david/Documents/workspace/web/assisted_cjk_reader/web_app/pdfbox/out/artifacts/main_jar/main.jar";

var app;
var agent;

var async = require('async');
var path = require('path');
var fs = require('fs');


var test_upload_path = path.join(__dirname, '../', 'test_files', 'test_upload.pdf');

var data = {
    "filename": "test_name",
    "filetype": "test_type",
    "filesize": "test_size",
    "file_base64": null
};

var mongo_client = require('mongodb').MongoClient;

var db;

function drop_test_users_and_uploads(done) {
    db.collection('users').drop(function (err, response) {
        db.collection('uploads').drop(function (err, response) {
            done();
        });
    });
}




describe('upload download endpoints', function () {


    before('init', function (done) {
        console.log('upload_download_test: BEFORE STARTED');
        app = require('../../../app');
        agent = request(app);
        var file_data = fs.readFileSync(test_upload_path);
        data.file_base64 = file_data.toString('base64');

        mongo_client.connect('mongodb://localhost:27017/cjk_reader_test', function (err, database) {
            if (err == null) {
                db = database;
                drop_test_users_and_uploads(done);
            } else {
                console.log("error: " + err);
            }
        });

    });


    var session_cookie;
    var upload_id;
    var new_account = {
        username: 'david',
        password: 'password'
    };

    before('signup and login', function (done) {
        async.waterfall([
            function (callback) {
                agent.post('/user/signup').send(new_account).end(function (err, res) {
                    if (res.status != 200) console.log(res.status);
                    assert(res.status == 200);
                    callback(err);
                });
            },
            function (callback) {
                agent.post('/user/login').send(new_account).end(function (err, res) {
                    if (res.status != 200) console.log(res.status);
                    session_cookie = res.headers['set-cookie'][0];
                    assert(res.status == 200);
                    callback(err);
                });
            },
        ], function (err, res) {
            if (err) console.log(err);
            done();
        });
    });


    it('should receive new uploads successfully', function (done) {

        agent.post('/user/file/upload').send({ file: JSON.stringify(data) }).set('Cookie', session_cookie).end(function (err, res) {
            if (err != null) console.log(err);
            assert(err == null);
            assert(res.status == 200);
            console.log(res.body);
            upload_id = res.body.upload_id;
            done();
        });

    });


    it('should allow pages to be downloaded', function (done) {

        agent.get('/user/file/download').query({file_id: upload_id, page: 4}).set('Cookie', session_cookie).end(function (err, res) {
            if (err) console.log(err);
            assert(err == null);
            console.log("RESPONSE: " + JSON.stringify(res.body));
            //assert(res.body.response != null && res.body.response.num_of_pages != null && res.body.response.text != null);
            //console.log(new Buffer(res.body.response.text, 'base64').toString('utf-8'));
            done();
        });

    });
    
    it('should alert users if requested file can\'t be downloaded', function (done) {

        var invalid_id = '';

        agent.get('/user/file/download').query({ file_id: invalid_id, page: 4 }).set('Cookie', session_cookie).end(function (err, res) {
            if (err) console.log(err);
            assert(res.status == 404);
            assert(res.body.response == null);
            done();
        });

    });

});