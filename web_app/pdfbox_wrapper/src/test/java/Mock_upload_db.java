
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.Files;

public class Mock_upload_db implements Upload_db {

    @Override
    public void connect(String hostName, String dbName, String collectionName) {
        // do nothing
    }

    @Override
    public File get_file(String user_id, String file_id) {
        return get_sample_pdf(file_id);
    }

    private File get_tempFile() {
        File file = null;
        try {
            file = File.createTempFile("pattern", ".pdf");
        } catch (IOException e) {
            System.err.println("error creating tempFile");
            System.exit(1);
        } finally {
            if (file == null) {
                System.err.println("error creating temp file");
                System.exit(1);
            }
        }

        file.deleteOnExit();
        return file;
    }

    private File get_sample_pdf(String filename) {

        URL url = getClass().getResource(filename);
        File sample_pdf = new File(url.getPath());

        byte data [] = null;
        try {
            data = Files.readAllBytes(sample_pdf.toPath());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (data == null) {
                System.err.println("error reading pdf bytes");
            }
        }

        File new_file = get_tempFile();
        try (OutputStream stream = new FileOutputStream(new_file)) {
            stream.write(data);
        } catch (IOException e) {
            System.err.println("error reading file_data");
            new_file = null;
        }

        return sample_pdf;

    }

}
