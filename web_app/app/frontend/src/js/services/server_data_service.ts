

export default function ($http: ng.IHttpService) {
    
    this.query_mecab = query_mecab;
    this.query_dictionary = query_dictionary;
    this.get_fileList = get_fileList;
    this.signup = signup;
    this.login = login;
    this.logout = logout;
    this.load_page = load_page;
    this.delete_file = delete_file;
    this.upload_file = upload_file;

    function query_mecab (japanese_text, format) {
        return $http({
            method: "GET",
            url: "/api/parse",
            cache: true,
            params: { text_in: japanese_text.trim(), format: format }
        });
    };

    function query_dictionary (text, in_json: Boolean) {
        return $http({
            method: "GET",
            url: "/api/dictionary",
            cache: true,
            params: { word: text, in_json: in_json }
        });
    };

    function get_fileList () {
        return $http({
            method: "GET",
            url: "/user/file/file_list",
            cache: false
        });
    };

    function signup (username, password) {
        return $http({
            method: "POST",
            url: "/user/signup",
            cache: false,
            params: { username: username, password: password },
        });
    }

    function login (username, password) {
        return $http({
            method: "POST",
            url: "/user/login",
            cache: false,
            params: { username: username, password: password },
        });
    }

    function logout () {
        return $http({
            method: "POST",
            url: "/user/logout",
            cache: false,
        });
    }
    

    function load_page (file_id, page) {
        return $http({
            method: "GET",
            url: "/user/file/download",
            cache: true,
            params: { file_id: file_id, page: page },
        });
    }

    function delete_file (file_id) {
        return $http({
            method: "POST",
            url: "/user/file/delete",
            cache: false,
            params: { file_id: file_id },
        });
    }

    function upload_file (data) {
        return $http({
            method: "POST",
            url: "/user/file/upload",
            cache: false,
            data: { file: JSON.stringify(data) },
        });
    }


};