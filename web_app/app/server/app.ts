
import * as express from "express";
import path = require('path');
//var favicon = require('serve-favicon');
import * as logger from "morgan";
var cookieParser = require('cookie-parser');

import * as session from "express-session";
import * as passport from "passport";
import * as RedisStore from "connect-redis";
import * as bodyParser from "body-parser";

import { PdfboxService } from "./util/pdfbox_service";
import { MecabService } from "./util/mecab_service";
import * as config from "./config";
import DbManager from "./db/DbManager";
import { configure_passport } from "./db/passport";
import * as routes from "./routing";
var redis_store = RedisStore(session);

import { Database, IMecab, Pdfbox } from "Server";

//var public_directory = path.resolve(__dirname, "..", "..", 'frontend', 'public');
//var views_directory = path.resolve(__dirname, 'views');
var root_dir = process.env.APP_ROOT_DIR;
var public_directory = path.join(root_dir, "frontend/public");
var views_directory = path.join(root_dir, "server/dist/views");


export class Server {

  public app: express.Application;
  private db: Database;
  private mecab_service: IMecab;
  private pdfbox_service: Pdfbox;
  private config;
  
  constructor(database?: Database, mecab_service?: IMecab, pdfbox_service?: Pdfbox) {
    this.app = express();
    this.config = config;
    
    if (database) this.db = database;
    else this.db = new DbManager();
    this.db.connect((err: Error) => {
      if (err) console.log(err);
      else console.log("db connected successfully");
    });

    if (mecab_service) this.mecab_service = mecab_service;
    else this.mecab_service = new MecabService();

    if (pdfbox_service) this.pdfbox_service = pdfbox_service;
    else this.pdfbox_service = new PdfboxService();
    this.pdfbox_service.connect(process.env.PDFBOX_PORT, config.get_mongodb_hostName(), "cjk_reader");

    this.configure_app();

  }

  private configure_app(): void {

    // hides server name for security
    this.app.disable('x-powered-by');

    // provide db, mecab, and pdfbox to all routes
    this.app.all('*', (request, response, next) => {
      request.db = this.db;
      request.mecab_service = this.mecab_service;
      request.pdfbox_service = this.pdfbox_service;
      next();
    });

    this.app.all('/', function (req, res, next) {
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
      next();
    });

    // view engine setup
    this.app.set('views', views_directory);
    this.app.set('view engine', 'ejs');

    // uncomment after placing your favicon in /public
    // app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
    this.app.use(logger('dev'));
    this.app.use(bodyParser.json({ limit: '100mb' }));
    this.app.use(bodyParser.urlencoded({ limit: '128mb', extended: true }));
    this.app.use(cookieParser());

    this.app.use(session({
      cookie: {
        path: '/',
        httpOnly: true,
        secure: false,
        maxAge: 3 * 60 * 60 * 1000
      },
      resave: true,
      saveUninitialized: true,
      secret: this.config.get_session_secret(),
      store: new redis_store({ url: this.config.get_redis_url(), ttl: 3 * 60 * 60 }),
      rolling: true
    }));

    this.app.use(passport.initialize());
    this.app.use(passport.session()); // persistent login sessions
    // this.app.use(flash()); // use connect-flash for flash messages stored in session
    configure_passport(passport, this.db);

    this.app.use(express.static(public_directory));
    this.app.use(routes.get_routing(passport));


    // catch 404 and forward to error handler
    this.app.use(function (req, res, next) {
      var err = new Error('Not Found');
      err.status = 404;
      next(err);
    });

    // error handlers

    // development error handler
    // will print stacktrace
    if (this.app.get('env') === 'development') {
      this.app.use(function (err: Error, req: express.Request, res: express.Response, next: express.NextFunction) {
        res.status(err.status || 500);
        res.render('error', {
          message: err.message,
          error: err
        });
      });
    }

    // production error handler
    // no stacktraces leaked to user
    this.app.use(function (err: Error, req: express.Request, res: express.Response, next: express.NextFunction) {
      res.status(err.status || 500);
      res.render('error', {
        message: err.message,
        error: {}
      });
    });

  }

}