
import MongooseService from "./MongooseService";
import * as configuration from "../config";

import { Database } from "Server";

export default class DbManager implements Database {

    private mongoose_service: MongooseService;
    private configuration;

    constructor () {
        this.mongoose_service = new MongooseService();
        this.configuration = configuration;
    }

    public connect (done: (err: Error) => void): void {
        this.mongoose_service.connect(this.configuration.get_mongoose_url(), (err: Error) => {
            done(err);
        });
    }

    public close(done: () => void): void {
        
        this.mongoose_service.close(() => {
            done();
        })

        /*
        parallel({
            mongoose: function (callback) {
                this.mongoose_service.close(callback);
            },
            mongoclient: function (callback) {
                this.mongoclient_service.close(callback);
            }
        }, function (err) {
            done();
        });
        */
    }

    public save_upload (user_id: string, file: any, callback: (err, updated_uploads) => void): void {
        this.mongoose_service.save_upload(user_id, file, callback);
    }

    public delete_upload (user_id: string, file_id: String, callback: (err, updated_uploads) => void): void {
        this.mongoose_service.delete_upload(user_id, file_id, callback);
    }

    public get_user_file_list (user_id: string, callback: (err, user_uploads) => void): void {
        this.mongoose_service.get_user_file_list(user_id, callback);
    }

    public get_jmdict_entry (word: string, callback: (err, docs) => void): void {
        this.mongoose_service.get_jmdict_entry(word, callback);
    }

    public create_user (username, password, callback: (err, new_user) => void): void {
        this.mongoose_service.create_user(username, password, callback);
    }

    public get_user_by_id (id, callback: (err, user) => void): void {
        this.mongoose_service.get_user_by_id(id, callback);
    }

    public get_user_by_username (username, callback: (err, user) => void): void {
        this.mongoose_service.get_user_by_username(username, callback);
    }

}