
export function get_mongoose_url() {
    if (process.env.env == 'development') {
        return 'mongodb://localhost:27017/cjk_reader';
    } else if (process.env.env == 'production') {
        return 'mongodb://mongodb:27017/cjk_reader';
    } else if (process.env.env == 'test') {
        return 'mongodb://localhost:27017/cjk_reader_test';
    }
};

export function get_mongoClient_url() {
    if (process.env.env == 'development') {
        return 'mongodb://localhost:27017/cjk_reader';
    } else if (process.env.env == 'production') {
        return 'mongodb://mongodb:27017/cjk_reader';
    } else if (process.env.env == 'test') {
        return 'mongodb://localhost:27017/cjk_reader';
    }
};

export function get_mongodb_hostName () {
    if (process.env.env == 'development') {
        return 'localhost';
    } else if (process.env.env == 'production') {
        return 'mongodb';
    } else {
        console.error("could not read process.env.env");
        return null;
    }
}