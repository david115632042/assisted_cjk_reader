
import my_navbar from "../../src/js/my_navbar";
import signup_modal from "../../components/signup-modal/signup-modal";
import login_modal from "../../components/login-modal/login-modal";
import profile_badge from "../../components/profile-badge/profile-badge";

export default function (app) {

    app.directive("myNavbar", my_navbar);
    app.directive("signupModal", signup_modal);
    app.directive("loginModal", login_modal);
    app.directive("profileBadge", profile_badge);

}