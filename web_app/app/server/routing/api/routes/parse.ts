var express = require('express');
var router = express.Router();

export function get_routing() {

    router.get('/', function (req, res, next) {

        var text = req.param('text_in');
        var format = req.param('format') || 'pretty';

        req.mecab_service.parse(text, function (err: Error, tokens) {
            console.log("err is: " + err);
            console.log("text is: " + text);
            console.log(JSON.stringify(tokens));

            if (format == 'pretty') {
                res.json(req.mecab_service.format_mecab(tokens));
            } else if (format == 'raw') {
                res.json(tokens);
            } else if (format == 'rendered') {
                res.render('analyzer_segmented', {
                    words_array: req.mecab_service.format_mecab(tokens),
                    get_word_class: req.mecab_service.get_word_class
                });
            }

        });
    });

    return router;

}