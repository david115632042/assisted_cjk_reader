
import * as express from "express";
var router = express.Router();

import require_auth from "../require_authentication";

export function get_routing () {

    router.post('/', require_auth, function (req, res) {
        req.logout();
        res.send('logged out successfully');
    });

    return router;

}

