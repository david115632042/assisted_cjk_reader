#!/bin/bash

if [ "$environment" = "production" ]
then
    ln -s /etc/nginx/sites-available/nginx_server_conf /etc/nginx/sites-enabled/nginx_server_conf
    echo "Starting nginx in docker network"
    ls /etc/nginx/sites-enabled
    service nginx start
elif [ "$environment" = "development" ]
then
    ln -s /etc/nginx/sites-available/nginx_local_conf /etc/nginx/sites-enabled/nginx_local_conf
    echo "Starting nginx in host mode"
    ls /etc/nginx/sites-enabled
    service nginx start
else
    echo "Could not read environment variable"
fi