
export default function($cookies: ng.cookies.ICookiesService) {

    // sidebar is always visible at min-width: 992px
    var sidebar_query = window.matchMedia("(min-width: 992px)");
    
    sidebar_query.addListener((width_query: MediaQueryList) => {
        this.sidebar_visible = width_query.matches;
        this.sidebar_always_visible = width_query.matches;
    });

    this.sidebar_always_visible = sidebar_query.matches;
    this.sidebar_visible = sidebar_query.matches;

    this.logged_in = $cookies.getObject("logged_in") ? $cookies.getObject("logged_in").status : false;
    this.username = $cookies.getObject("username") ? $cookies.getObject("username").username : false;

    this.alert_logged_in = (username: String) => { 
        this.logged_in = true; 
        this.username = username;
        $cookies.putObject("logged_in", {status: true});  
        $cookies.putObject("username", {username: username});  
    };

    this.alert_logged_out = () => { 
        this.logged_in = false; 
        this.username = false;
        $cookies.putObject("logged_in", {status: false});
        $cookies.putObject("username", {username: false});
    };

    this.alert_sidebar_visible = () => {
        this.sidebar_visible = true;
    };

    this.alert_sidebar_hidden = () => {
        this.sidebar_visible = false;
    };

}