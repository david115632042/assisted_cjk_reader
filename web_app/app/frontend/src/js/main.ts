
import * as $ from "jquery";
window.$ = $;
window.jQuery = $;

import "angular";
import "angular-ui-router";
import "angular-cookies";

import "toastr/toastr.scss";
import * as toastr from "toastr";
window.toastr = toastr;

import "bootstrap";
require("bootstrap/dist/css/bootstrap.css");
require("font-awesome/scss/font-awesome.scss");
require("../sass/style.scss");

var app = angular.module("app", [
    "ui.router",
    "ngCookies"
]);

import welcome from "../welcome/welcome";
app.controller("welcome_controller", welcome);

import routing from "./routing";
app.config(routing);

import server_data from "./services/server_data_service";
app.service("server_data", server_data);

import display_state from "./services/display_state_service";
app.service("display_state", display_state);

import common_components_initialize from "../common_components/dist/index";
common_components_initialize(app);

import analyzer_initialize from "../analyzer/dist/js/index";
analyzer_initialize(app);

import navbar_initialize from "../navbar/dist/js/index";
navbar_initialize(app);

import sidebar_initialize from "../sidebar/dist/js/index";
sidebar_initialize(app);

import online_resources_controller from "../online_resources/online_resources";
app.controller("online_resources_controller", online_resources_controller);
