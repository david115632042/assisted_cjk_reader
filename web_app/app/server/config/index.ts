
import * as mongodb from "./mongodb";
import * as redis from "./redis";
import * as sessions from "./sessions";

export function get_mongoose_url () {
    return mongodb.get_mongoose_url();
}

export function get_mongoclient_url () {
    return mongodb.get_mongoClient_url();
}

export function get_mongodb_hostName () {
    return mongodb.get_mongodb_hostName();
}

export function get_redis_url () {
    return redis.get_url();
}

export function get_session_secret () {
    return sessions.session_secret;
}
