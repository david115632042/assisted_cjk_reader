import * as express from "express";
var files = express.Router();

import * as download from "./routes/download";
import * as upload from "./routes/upload";
import * as delete_router from "./routes/delete";
import * as file_list from "./routes/file_list";


export function get_routing () {

    files.use('/download', download.get_routing());
    files.use('/upload', upload.get_routing());
    files.use('/delete', delete_router.get_routing());
    files.use('/file_list', file_list.get_routing());

    return files;

}