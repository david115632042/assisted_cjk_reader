
var MecabClass = require('mecab-lite');

import {Mecab} from "Api";
import {IMecab, MecabRawToken} from "Server";

export class MecabService implements IMecab {

    private mecab;
    private grammar_dictionary;

    constructor() {
        this.mecab = new MecabClass();
        this.mecab.MECAB = '/usr/bin/mecab';
        this.mecab.ENCODING = 'UTF-8';

        this.grammar_dictionary = {
            品詞: "part of speech",
            助詞: "particle",
            名詞: "noun",
            代名詞: "pronoun",
            動詞: "verb",
            他動詞: "transitive verb",
            自動詞: "intransitive verb",
            助動詞: "auxiliary verb",
            形容詞: "adjective",
            副詞: "adverb",
            前置詞: "preposition",
            接続詞: "conjunction",

            連体形: "attributive form",
            連用形: "conjunctive form",
            
            接続助詞: "conjunctive particle",

            接尾: "suffix"
        }

    }

    public parse(input: String, callback: (err: Error, tokens: MecabRawToken[]) => void) {
        this.mecab.parse(input, function (err, items: MecabRawToken[]) {
            callback(err, items);
        });
    }

    private join_conjunctive(root: MecabRawToken, root_index: number, tokens: Mecab.MecabToken[], token_index: number) {

    }

    public format_mecab(mecab_output: MecabRawToken[]): Mecab.MecabToken[] {
        
        var mecab_tokens: Mecab.MecabToken[] = [];
        mecab_output.forEach((value: string[], index: number, array: string[][]) => {
            mecab_tokens[index] = this.build_token_object(value);
        })

        var return_tokens: Mecab.MecabToken[] = [];

        for (var i = 0; i < mecab_tokens.length; i++) {

            var current_token = mecab_tokens[i];

            // break if end of stream
            if (current_token.surface_form === "EOS" &&
                    current_token.part_of_speech.primary == null) break;

            // initialize full_form
            current_token.full_form = current_token.surface_form; 

            // join verbs with linking auxiliary verbs
            if (current_token.part_of_speech.primary === "verb") {

                // loop over following tokens
                for (var j = i + 1; j < mecab_tokens.length; j++) {

                    var next_token = mecab_tokens[j];

                    // check if token is an auxiliary verb, 
                    if (next_token.part_of_speech.primary === "auxiliary verb"
                            || next_token.part_of_speech.subtypes.indexOf("suffix") > -1) {
                        current_token.full_form += next_token.surface_form;
                        current_token.joined_tokens.push(next_token);
                        i++;
                    } else {
                        // otherwise break loop
                        break;
                    }

                }

            }

            return_tokens.push(current_token);

        }

        return return_tokens;

    }


    private build_token_object(token: MecabRawToken): Mecab.MecabToken {

        // swap out pos tags with english translations
        for (var i = 1; i < 7; i++) {
            token[i] = this.grammar_dictionary[token[i]] ? this.grammar_dictionary[token[i]] : token[i];
        }

        var formatted_token: Mecab.MecabToken = {
            surface_form: token[0],
            part_of_speech: {
                primary: token[1], 
                subtypes: [token[2], token[3], token[4]]
            },
            inflection: token[5],
            conjugation: token[6],
            root_form: token[7],
            reading: token[8],
            pronounciation: token[9],
            full_form: null,
            joined_tokens: []
        };
       
        return formatted_token;
    }

}
