import * as express from "express";
var views = express.Router();

import * as navbar from "./routes/navbar";

export function get_routing () {

    views.use('/navbar', navbar.get_routing());

    return views;

}