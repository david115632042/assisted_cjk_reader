
import { DisplayState, ServerData } from "Client";

export default function ($rootScope, server_data: ServerData, display_state: DisplayState) {
    return {
        restrict: "E",
        scope: {
            trigger_load_file: "&onLoadFile",
            trigger_delete_file: "&onDeleteFile"
        },
        template: require("./file_viewer.html"),
        link: function ($scope, elem, attr) {

            $scope.logged_in = display_state.logged_in;
            $scope.online_files = [];

            // init
            update_file_list();

            $scope.get_formatted_file_size = get_formatted_file_size;

            $scope.on_load_clicked = on_load_clicked;

            $scope.on_delete_clicked = on_delete_clicked;

            $rootScope.$on("file_list_updated", on_fileList_updated);

            $scope.$watch(() => { return display_state.logged_in; }, on_logged_in_updated);

            function update_file_list(): void {
                server_data.get_fileList().then(function successCallback(file_list) {
                    $scope.online_files = file_list.data.file_list;
                }, function errorCallback(file_list) {
                    if (file_list.status === 401) {
                        display_state.alert_logged_out();
                    }
                });
            }


            function get_formatted_file_size(num_of_bytes): String {
                if (num_of_bytes > 1000000) {
                    return (num_of_bytes / 1000000).toFixed(2).toString() + "MBytes";
                } else if (num_of_bytes > 1000) {
                    return (num_of_bytes / 1000).toFixed(2).toString() + "KBytes";
                }
            }


            function on_load_clicked (index): void {
                var file_id = $scope.online_files[index]._id;
                $scope.trigger_load_file()(file_id);
            }


            function on_delete_clicked(index): void {
                var file_id = $scope.online_files[index]._id;
                $scope.trigger_delete_file()(file_id);
            }


            function on_fileList_updated (event, data): void {
                update_file_list();
            }


            function on_logged_in_updated (logged_in): void {
                $scope.logged_in = logged_in;
                if (logged_in) {
                    update_file_list();
                } else {
                    $scope.online_files = [];
                }
            }
        }
    };
};