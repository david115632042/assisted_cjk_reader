
export default function ($scope, $state) {
    var vm = this;

    vm.$state = $state;
    vm.words_array = [];
    vm.input_user = {};
    vm.input_user.analyzer_input = "";

    vm.load_text = load_text;

    function load_text() {
        var text_in = vm.input_user.analyzer_input;

        var output_element = $("#output");

        if (text_in.length === 0) {
            toastr.error("no input detected");
        } else {
            $state.go("analyzer_output", { input_text: text_in });
        }
    }

};
