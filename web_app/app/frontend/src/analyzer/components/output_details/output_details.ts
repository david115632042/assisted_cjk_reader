
import { ServerData } from "Client";

export default function (server_data: ServerData) {
    return {
        restrict: "E",
        replace: false,
        template: require("./output_details.html"),
        scope: {
            external_controller: "=externalController"
        },
        link: function ($scope, elem, attrs) {

            $scope.external_controller = $scope.external_controller || {};
            $scope.external_controller.load_details = load_details;

            $scope.details_visible = false;

            $scope.show_details = function () {
                $scope.details_visible = true;
            };

            $scope.toggle_details = function () {
                $scope.details_visible = ! $scope.details_visible;
            };

            $scope.$on("load_details", function (e, data) {
                $scope.is_loading = true;
                $scope.details_visible = true;

                server_data.query_dictionary(data.target, true).then(function success (response) {
                    $scope.entry_list = response.data;
                    $scope.is_loading = false;
                }, function error (response) {
                    $scope.is_loading = false;
                });

            });

            function load_details (term: string) {
                $scope.is_loading = true;
                $scope.details_visible = true;

                server_data.query_dictionary(term, true).then(function success (response) {
                    $scope.entry_list = response.data;
                    $scope.is_loading = false;
                }, function error (response) {
                    $scope.is_loading = false;
                });
            }

        }
    };
}