
public class Parse_args {

    public static Args parse(String[] args) {

        String command = null;
        String session_id = null;
        String filename = null;
        int start_page = -1;
        int end_page = -1;

        if (args[0].equals("extract")) {

            command = "extract";
            for (int i = 1; i < args.length; i++) {

                if (args[i].equals("--user_id")) {
                    session_id = args[i + 1];
                    i++;
                } else if (args[i].equals("--file_id")) {
                    filename = args[i + 1];
                } else if (args[i].equals("--pages")) {
                    start_page = Integer.parseInt(args[i + 1]);
                    end_page = Integer.parseInt(args[i + 2]);
                    i += 2;
                }

            }

        } else if (args[0].equals("pages")) {

            command = "pages";
            for (int i = 1; i < args.length; i++) {

                if (args[i].equals(("--user_id"))) {
                    session_id = args[i + 1];
                } else if (args[i].equals("--file_id")) {
                    filename = args[i + 1];
                }

            }

        } else {
            System.err.println("available commands are: extract, pages");
            System.exit(-1);
        }

        if (command.equals("extract")) {
            if (session_id == null || filename == null ||
                    start_page == -1 || end_page == -1) {
                System.err.println("error parsing extract command");
                System.exit(-1);
            }
        } else if (command.equals("pages")) {
            if (session_id == null || filename == null) {
                System.err.println("error parsing pages command");
                System.exit(-1);
            }
        } else {
            System.err.println("valid command not detected");
            System.exit(-1);
        }

        Args return_args = new Args();
        return_args.command = command;
        return_args.user_id = session_id;
        return_args.file_id = filename;
        return_args.start_page = start_page;
        return_args.end_page = end_page;

        return return_args;

    }

}
