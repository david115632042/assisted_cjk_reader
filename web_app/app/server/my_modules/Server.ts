

declare module "Server" {

    import * as mongoose from "mongoose";
    import { Jmdict, Mecab } from "Api";


    export interface Database {
        connect(done: (err: Error) => void): void;
        close(done: () => void): void;

        save_upload(user_id: string, file: any, callback: (err, updated_uploads) => void): void;
        delete_upload(user_id: string, file_id: string, callback: (err, updated_uploads) => void): void;
        get_user_file_list(user_id: string, callback: (err, user_uploads: dbType.UserDoc) => void): void;

        get_jmdict_entry(
            word: string,
            callback: (err, docs: Jmdict.JMdictEntry[]) => void
        ): void;

        create_user(username, password, callback: (err, new_user) => void): void;
        get_user_by_id(id, callback: (err, user) => void): void;
        get_user_by_username(username, callback: (err, user) => void): void;
    }

    export interface MongooseService {
        connect(url, done: (err: Error) => void): void;
        close(done: () => void);
        save_upload(user_id, file: dbType.File, callback: (err: string, updated_uploads: any) => void);
        delete_upload(user_id, file_id, callback: (err: string, user_uploads: any) => void);
        get_user_file_list(user_id, callback: (err: string, user_uploads: any) => void);
        create_user(username, password, callback: (err, new_user) => void);
        get_jmdict_entry(
            word: string,
            callback: (err, docs: Jmdict.JMdictEntry[]) => void
        ): void;
    }

    export interface IMecab {
        parse(input: string, callback: (err: Error, tokens: Array<MecabRawToken>) => void): void;
        format_mecab(mecab_output: Array<MecabRawToken>): Mecab.MecabToken[];
    }

    export type MecabRawToken = Array<string>;

    export interface Pdfbox {
        connect(port, hostName, dbName);
        extract_pdf_text(user_id, file_id, start, end, done);
    }

    export namespace dbType {

        export interface File {
            filename: string;
            filetype: string;
            filedata: string;
        }

        export interface FileDoc extends File, mongoose.Document { }

        export interface Uploads {
            user_id: string;
            file_list: dbType.File[];
        }

        export interface UploadsDoc extends Uploads, mongoose.Document { }

        export interface User {
            local: any;
            facebook: any;
            twitter: any;
            google: any;
            files: dbType.File[]
            generateHash: (password) => any;
            validPassword: boolean;
        }

        export interface UserDoc extends User, mongoose.Document { }

        export interface JmDictEntry extends Jmdict.JMdictEntry { }
        export interface JmDictDoc extends Jmdict.JMdictEntry, mongoose.Document { }

    }
    

}

