
import { ServerData, DisplayState } from "Client";

export default function(server_data: ServerData, display_state: DisplayState) {

    return {
        restrict: "E",
        scope: true,
        template: require("./login-modal.html"),
        link: function( $scope, elem, attrs ) {
            $scope.username = "";
            $scope.password = "";

            $scope.submit = submit;

            function submit () {
                var submitted_username = $scope.username;
                server_data.login(submitted_username, $scope.password).then(function (response) {
                    console.log(JSON.stringify(response));
                    if (response.status === 200) {
                        toastr.success("logged in successfully");
                        
                        // delay login event untill after modal hiding animation ends
                        $("#login-modal").on("hidden.bs.modal", function () {
                            display_state.alert_logged_in(submitted_username);
                            $scope.$apply();
                        });
                        $("#login-modal").modal("hide");
                        
                    } else {
                        toastr.error(response.headers("www-authenticate"));
                    }
                });
            }

        }
    }
    
};