
import file_viewer from "../../components/file_viewer/file_viewer";
import file_selector from "../../components/file_selector/file_selector";
import output_details from "../../components/output_details/output_details";

import analyzer_input_controller from "../../src/js/analyzer_input_controller";
import analyzer_input_text_controller from "../../src/js/analyzer_input_text_controller";
import analyzer_input_file_controller from "../../src/js/analyzer_input_file_controller";
import analyzer_input_online_resource_controller from "../../src/js/analyzer_input_online_resource_controller";

import analyzer_output_controller from "../../src/js/analyzer_output_controller";
import analyzer_output_text_controller from "../../src/js/analyzer_output_text_controller";
import analyzer_output_file_controller from "../../src/js/analyzer_output_file_controller";
import analyzer_output_online_resource_controller from "../../src/js/analyzer_output_online_resource_controller";

export default function(app) {

    app.directive("fileViewer", file_viewer);
    app.directive("fileSelector", file_selector);
    app.directive("outputDetails", output_details);

    app.controller("analyzer_input_controller", analyzer_input_controller);
    app.controller("analyzer_input_text_controller", analyzer_input_text_controller);
    app.controller("analyzer_input_file_controller", analyzer_input_file_controller);
    app.controller("analyzer_input_online_resource_controller", analyzer_input_online_resource_controller);

    app.controller("analyzer_output_controller", analyzer_output_controller);
    app.controller("analyzer_output_text_controller", analyzer_output_text_controller);
    app.controller("analyzer_output_file_controller", analyzer_output_file_controller);
    app.controller("analyzer_output_online_resource_controller", analyzer_output_online_resource_controller);

}

