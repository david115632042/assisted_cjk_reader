
import { Mecab } from "Api";

export default function($rootScope, $state, $stateParams, server_data) {
    var vm = this;

    vm.output_details_controller = {};

    vm.input = {};
    
    vm.query_mecab = query_mecab;
    vm.load_page = load_page;
    vm.load_details = load_details;

    vm.pressed_last_page = pressed_last_page;
    vm.pressed_next_page = pressed_next_page;
    vm.pressed_go_to_page = pressed_go_to_page;

    vm.current_page_num = Number($stateParams.page);

    load_page($stateParams.input_file_id, $stateParams.page);

    vm.set_word_class = set_word_class;

    function query_mecab (text_in): void {
        if (text_in.length === 0) { return; }
        vm.loading = true;

        server_data.query_mecab(text_in, "pretty").then(function success (response) {
            vm.words_array = response.data;
            vm.loading = false;
        }, function error (response) {
            vm.loading = false;
        });
    }

    function load_page (file_id, page): void {
        
        vm.loading = true;

        server_data.load_page(file_id, page).then(function success (response) {

            vm.loading = false;
            vm.words_array = [];
            vm.max_page_num = response.data.pages;

            var break_obj = function () { return { surface_form: "。  ", part_of_speech: { primary: "Particle" } }; };
            response.data.text.forEach(function (sentence) {

                sentence.forEach(function (term) {
                    vm.words_array.push(term);
                }, this);
                vm.words_array.push(break_obj());

            }, this);

        }, function error (response) {
            vm.loading = false;
            toastr.error("could not complete request");
        });

    }

    function load_details (token: Mecab.MecabToken): void {
        var word = (token.root_form && token.root_form !== "*") ? token.root_form : token.surface_form;
        vm.output_details_controller.load_details(word);
    }

    function go_to_page (page): void {
        $state.go("analyzer_output.file", { input_file_id : $stateParams.input_file_id, page: page });
    }

    function pressed_last_page (): void {
        go_to_page((Number($stateParams.page) - 1).toString());
    }

    function pressed_next_page (): void {
        go_to_page((Number($stateParams.page) + 1).toString());
    }

    function pressed_go_to_page (): void {
        go_to_page(vm.current_page_num);
    }

    function set_word_class (word: Mecab.PosMember): string {
        if (word === "noun") {
            return "output-word-N";
        } else if (word === "particle") {
            return "output-word-Pa";
        } /*else if (word === "symbol") {
            return "output-word-Sy";
        }*/ else if (word === "adjective") {
            return "output-word-Adj";
        } else if (word === "verb") {
            return "output-word-V";
        } else if (word === "adverb") {
            return "output-word-Adv";
        } else if (word === "auxiliary verb") {
            return "output-word-AuxV";
        }
    }

};