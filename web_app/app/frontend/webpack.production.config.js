var webpack = require('webpack');

var autoprefixer = require('autoprefixer');

var path = require('path');

module.exports = {

    context: __dirname,
    entry: "./src/js/main.ts",
    cache: false,
    debug: false,
    devtool: "cheap-source-map",
    output: {
        path: path.resolve(__dirname, "public", "js"),
        filename: "bundle.js"
    },
    resolve: {
        extensions: ['', '.webpack.js', '.web.js', '.ts', '.tsx', '.js'],

        modulesDirectories: [
            "web_modules",
            "node_modules",
            "my_modules"
        ]
    },
    plugins: [
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: true
            },
            mangle: false,
            sourceMap: false
        })
    ],
    module: {

        loaders: [

            {
                test: /\.jsx?$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel'
            },
            {
                test: /\.ts$/,
                loader: 'ts-loader'
            },
            {
                test: /\.css$/,
                loader: 'style-loader!css-loader?sourceMap!postcss-loader'
            },
            {
                test: /\.scss$/,
                loaders: ["style", "css", "postcss-loader", "sass"]
            },
            {
                test: /\.html$/,
                loader: "html-loader"
            },
            {
                test: /\.(eot|svg|ttf|woff(2)?)(\?v=\d+\.\d+\.\d+)?/,
                loader: 'file-loader?name=../fonts/[hash].[ext]'
            }

        ]

    },
    postcss: function () {
        return [autoprefixer];
    }
};