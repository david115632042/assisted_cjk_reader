import { expect, assert } from "chai";
import * as mongoose from "mongoose";
import MongooseService from "../../db/MongooseService";
import { dbType } from "Server";

var mongooseService = new MongooseService();
var test_mongo_url = 'mongodb://localhost:27017/test';
var connection = mongoose.createConnection(test_mongo_url);


before("connect to mongo", (done) => {
    mongooseService.connect(test_mongo_url, (err) => {
        done();
    });
});

before((done) => {
    connection.db.dropDatabase().then(function success() {
        done();
    });
});

describe("mongooseService", () => {

    it("should return dictionary results", (done) => {

        mongooseService.get_jmdict_entry("いけない", (err, docs) => {
            if (err) console.log(err);
            console.log("DOCS: " + JSON.stringify(docs));

            done();
        });

    });

    it("should register new users", (done) => {
        mongooseService.create_user("david11563", "password", (err, new_user) => {
            console.log(JSON.stringify(err));
            console.log(JSON.stringify(new_user));
            done();
        });
    });


    it("should be able to store files for users", (done) => {
        var newFile: dbType.File = {
            filename: "file.txt",
            filetype: "txt",
            filedata: "0123456789"
        }
        mongooseService.save_upload("david11563", newFile, (err, updated_uploads) => {
            assert.isNull(err);
            done();
        });
    });

    it("should be able to retrieve a user's file list", (done) => {
        mongooseService.get_user_file_list("david11563", (err, user) => {
            console.log("USER LIST: " + JSON.stringify(user, null, '\t'));
            assert.isNull(err);
            assert.equal(user.fileList.length, 1, "returns a fileList array of one file");
            assert.isNumber(user.spaceUsed, "should include current space used in bytes");
            assert.isString(user.fileList[0].filename, "file should include it's filename");
            done();
        });
    });

    it("should be able to remove a file from the user's file list", (done) => {
        mongooseService.get_user_file_list("david11563", (err, user) => {
            console.log(JSON.stringify(user, null, '\t'));
            
            var files = user.fileList.filter(x => x.filename == "file.txt");
            console.log(JSON.stringify(files));
            //done();
            
            mongooseService.delete_upload("david11563", files[0]._id, (err, file) => {
                console.log("new user: " + JSON.stringify(file));
                done();
            });
            
            //done();
        });
        
    });
   
});
