var expect = require('chai').expect;
var assert = require('assert');

var async = require('async');

process.env.PDFBOX_DIR = '../../../../pdfbox_wrapper/out/artifacts/pdfbox_wrapper_jar/pdfbox_wrapper.jar';

var pdfbox_service_class = require('../../../server/util/pdfbox_service.js');

//var basic_command = "extract --user_id 578c7a96feec9d10009fc9a4 --file_id 57aa6240890b680f00c18507 --pages 0 1";
var user_id = '578c7a96feec9d10009fc9a4';
var file_id = '57aa6240890b680f00c18507';
var page_start = 0;
var page_end = 1;

var pdfbox_service;

before(function (done) {
    pdfbox_service = new pdfbox_service_class();
    pdfbox_service.connect(5000, 'localhost', 'cjk_reader');
    setTimeout(function() {
        done();
    }, 3000);
});

describe('pdfbox_service', function () {

    it('should perform parsing', function (done) {

        //pdfbox_service.connect(5000);
        pdfbox_service.extract_pdf_text(user_id, file_id, page_start, page_end, function (result) {
            console.log(result);
            assert(result != null);
            done();
        });

    });

    it('should return empty object when connection fails', function (done) {
        var unconnected_pdfbox_service = new pdfbox_service_class();
        unconnected_pdfbox_service.extract_pdf_text(user_id, file_id, page_start, page_end, function (result) {
            console.log(result);
            assert(result == null);
            done();
        });
    });
    
    it ('should handle multiple requests', function (done) {

        //pdfbox_service.connect(5000);

        async.parallel([
            function (cb) {
                pdfbox_service.extract_pdf_text(user_id, file_id, page_start, page_end, function (result) {
                    console.log(result);
                    cb(null, result);
                });
            },
            function (cb) {
                pdfbox_service.extract_pdf_text(user_id, file_id, page_start, page_end, function (result) {
                    console.log(result);
                    cb(null, result);
                });
            },
            function (cb) {
                pdfbox_service.extract_pdf_text(user_id, file_id, page_start, page_end, function (result) {
                    console.log(result);
                    cb(null, result);
                });
            },
            function (cb) {
                pdfbox_service.extract_pdf_text(user_id, file_id, page_start, page_end, function (result) {
                    console.log(result);
                    cb(null, result);
                });
            },
            function (cb) {
                pdfbox_service.extract_pdf_text(user_id, file_id, page_start, page_end, function (result) {
                    console.log(result);
                    cb(null, result);
                });
            },
            function (cb) {
                pdfbox_service.extract_pdf_text(user_id, file_id, page_start, page_end, function (result) {
                    console.log(result);
                    cb(null, result);
                });
            },
            function (cb) {
                pdfbox_service.extract_pdf_text(user_id, file_id, page_start, page_end, function (result) {
                    console.log(result);
                    cb(null, result);
                });
            },
            function (cb) {
                pdfbox_service.extract_pdf_text(user_id, file_id, page_start, page_end, function (result) {
                    console.log(result);
                    cb(null, result);
                });
            },
            function (cb) {
                pdfbox_service.extract_pdf_text(user_id, file_id, page_start, page_end, function (result) {
                    console.log(result);
                    cb(null, result);
                });
            }
        ],
        function (err, results) {
            //console.log(results);
            assert(results != []);
            var last_result = results[0];
            for (var i = 1; i < results.length; i++) {
                console.log(results[1]);
                assert(results[i].response.text == last_result.response.text);
                last_result = results[i];
            }
            done();
        });

        

    });
    
})