var webpack = require('webpack');

var autoprefixer = require('autoprefixer');
var nodeExternals = require('webpack-node-externals');
var path = require('path');

module.exports = {
    context: __dirname,
    entry: "./bin/entry.ts",
    target: 'node', // in order to ignore built-in modules like path, fs, etc. 
    externals: [nodeExternals()], 
    cache: true,
    debug: true,
    devtool: "source-map",
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "server.js"
    },
    resolve: {
        extensions: ['', '.webpack.js', '.web.js', '.ts', '.tsx', '.js'],

        // Configure path resolution strategy
        modulesDirectories: [
            "web_modules", 
            "node_modules", 
            "my_modules"
        ]
    },
    module: {

        loaders: [

            {
                test: /\.jsx?$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel'
            },
            {
                test: /\.ts$/,
                loader: 'ts-loader'
            },
            {
                test: /\.json$/,
                loader: 'json-loader'
            }

        ]

    }

};