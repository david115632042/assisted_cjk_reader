
import { ServerData, DisplayState } from "Client";

export default function(server_data: ServerData, display_state: DisplayState) {

    return {
        restrict: "E",
        template: require("./sidebar.html"),
        link: function($scope, elem, attrs) {
            
            $scope.sidebar_visible = display_state.sidebar_visible;
            $scope.sidebar_always_visible = display_state.sidebar_always_visible;
            $scope.username = display_state.username;

            $scope.show_sidebar = display_state.alert_sidebar_visible;
            $scope.hide_sidebar = display_state.alert_sidebar_hidden;
            $scope.logout = logout;

            $scope.$watch( () => { 
                return display_state.sidebar_visible; 
            }, (sidebar_visible) => {
                $scope.sidebar_visible = sidebar_visible;
            });

            $scope.$watch( () => { 
                return display_state.sidebar_always_visible; 
            }, (sidebar_always_visible) => {
                $scope.sidebar_always_visible = sidebar_always_visible;
                if (!sidebar_always_visible) display_state.alert_sidebar_hidden();
            });

            $scope.$watch( () => { 
                return display_state.username; 
            }, (username) => {
                $scope.username = username;
            });

            function logout() {
                server_data.logout().then(function successCallback(response) {
                    toastr.success("logged out");
                    display_state.alert_logged_out();
                }, function errorCallback(response) {
                    if (response.status === 401) {
                        toastr.success("logged out");
                        display_state.alert_logged_out();
                    } else {
                        toastr.error("error logging out");
                    }
                    
                });
            }

        }
    };

};