var expect = require('chai').expect;
var assert = require('assert');

var async = require('async');
var path = require('path');

var mongoose = require('mongoose');
mongoose.Promise = require('q').Promise;
var test_mongo_url = 'mongodb://localhost:27017/test';

var db_dir = '../../../server/db/';
var user_model = null;
var file_model = null;
var user_uploads_model = null;

function cleanup(callback) {

    async.parallel({
        user_model: function(next) {
            user_model.remove({}, function(err) {
                next();
            });
        },
        file_model: function(next) {
            file_model.remove({}, function(err) {
                next();
            });
        },
        user_uploads_model: function(next) {
            user_uploads_model.remove({}, function(err) {
                next();
            });
        }
        
    }, function(err, results) {
        callback();
    });

}

var mongoose_service;



describe('mongoose', function () {


    before('mongoose: connect', function (done) {
        console.log('mongoose_service_test: BEFORE STARTED');
        mongoose_service_class = require('../../../server/db/mongoose_service');
        mongoose_service = new mongoose_service_class();
        mongoose_service.connect(test_mongo_url, function () {

            user_model = mongoose_service.user_model;
            file_model = mongoose_service.file_model;
            user_uploads_model = mongoose_service.user_uploads_model;

            done();

        });

    });

    beforeEach('cleanup', function (done) {
        console.log('MONGOOSE_SERVICE: BEFORE_EACH STARTED');
        cleanup(done);
    });

    after('mongoose: close', function (done) {
        console.log('MONGOOSE:SERVICE: AFTER STARTED');
        mongoose_service.close(function () {
            done();
        });
    });


    describe('mongoose_service helpers', function () {

        describe('save_upload()', function () {

            it('should save file to new list successfully', function (done) {

                var mongoose_user_id = 'mongoose_user_id';
                var new_file = new file_model({ filename: 'new_file', filetype: 'new_type', filesize: 9000, filedata: 'aaaa' });

                async.waterfall([
                    function (callback) {
                        mongoose_service.save_upload(mongoose_user_id, new_file, function (err) {
                            callback(err);
                        });
                    },
                    function (callback) {
                        user_uploads_model.findOne({ user_id: mongoose_user_id }, function (err, uploads) {
                            callback(err, uploads);
                        });
                    }
                ], function (err, uploads) {
                    assert(err == null);
                    assert(uploads.file_list.length == 1);
                    assert(uploads.file_list[0].filename == 'new_file');
                    done();
                })
            });

            it('should update user uploads list successfully', function (done) {

                var mongoose_user_id = 'mongoose_user_id';
                var new_file = new mongoose_service.file_model({ filename: 'new_file', filetype: 'new_type', filesize: 9000, filedata: 'aaaa' });

                async.waterfall([
                    function (callback) {
                        mongoose_service.save_upload(mongoose_user_id, new_file, function (err) {
                            callback(err);
                        });
                    },
                    function (callback) {
                        var another_new_file = new file_model({ filename: 'another_new_file', filetype: 'another_new_type', filesize: 9000, filedata: 'aaaa' });
                        mongoose_service.save_upload(mongoose_user_id, another_new_file, function (err) {
                            callback(err);
                        });
                    },
                    function (callback) {
                        mongoose_service.user_uploads_model.findOne({ user_id: mongoose_user_id }, function (err, user_list) {
                            callback(err, user_list);
                        });
                    }
                ], function (err, user_list) {
                    assert(err == null);
                    assert(user_list.file_list.length == 2);
                    done();
                });
            });
        });

        describe('get_user_file_list()', function () {

            it('should return the file list for this user', function (done) {

                var mongoose_user_id = 'mongoose_user_id';
                var new_file = new file_model({ filename: 'new_file', filetype: 'new_type', filesize: 9000, filedata: 'aaaa' });
                var another_new_file = new file_model({ filename: 'another_new_file', filetype: 'another_new_type', filesize: 9000, filedata: 'aaaa' });

                async.waterfall([
                    function (callback) {
                        mongoose_service.save_upload(mongoose_user_id, new_file, function (err) {
                            callback(err);
                        });
                    },
                    function (callback) {
                        mongoose_service.save_upload(mongoose_user_id, another_new_file, function (err) {
                            callback(err);
                        });
                    },
                    function (callback) {
                        mongoose_service.get_user_file_list(mongoose_user_id, function (err, user_uploads) {
                            callback(err, user_uploads);
                        });
                    }
                ], function (err, user_uploads) {
                    assert(err == null);
                    assert(user_uploads.file_list.length == 2);
                    assert(user_uploads.file_list[user_uploads.file_list.length - 1].filedata == null);
                    done();
                });
            });
        });
    });

});


