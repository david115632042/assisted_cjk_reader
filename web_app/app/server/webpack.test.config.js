
var nodeExternals = require('webpack-node-externals');
 
module.exports = {
    target: 'node', // in order to ignore built-in modules like path, fs, etc. 
    externals: [nodeExternals()], // in order to ignore all modules in node_modules folder 
    resolve: {
        extensions: ["", ".ts", ".js", ".jsx"]
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                loaders: ['babel-loader']
            },
            {
                test: /(\.css|\.less)$/,
                loader: 'null-loader'
            },
            { 
                test: /\.ts$/, 
                loader: 'ts-loader' 
            }
        ]
    }
};


