import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.apache.commons.codec.binary.Base64;
import org.bson.Document;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import static com.mongodb.client.model.Filters.eq;

public class Mongo_upload_db implements Upload_db {

    public static String UPLOADS_COLLECTION = "uploads";

    private MongoDatabase db = null;
    private MongoCollection<Document> uploads_collection = null;

    private File get_tempFile() {
        File file = null;
        try {
            file = File.createTempFile("pattern", ".pdf");
        } catch (IOException e) {
            System.err.println("error creating tempFile");
            System.exit(1);
        } finally {
            if (file == null) {
                System.err.println("error creating temp file");
                System.exit(1);
            }
        }

        file.deleteOnExit();
        return file;
    }

    private void set_db_collection(String hostName, String dbName) {
        MongoClient mongoClient = new MongoClient(hostName, 27017);
        MongoDatabase db = mongoClient.getDatabase(dbName);
        uploads_collection = db.getCollection(UPLOADS_COLLECTION);
    }

    public void connect(String hostName, String dbName, String collection) {
        set_db_collection(hostName, dbName);
    }

    public File get_file(String user_id, String file_id) {

        Document session = uploads_collection.find(eq("user_id", user_id)).first();
        List<Document> file_list = (List<Document>)session.get("file_list");

        File file = get_tempFile();
        for (Document upload_file: file_list) {

            String current_file_id = upload_file.getObjectId("_id").toString();

            if (current_file_id.equals(file_id)) {
                String data_64 = upload_file.getString("filedata");
                byte data[] = Base64.decodeBase64(data_64);
                try (OutputStream stream = new FileOutputStream(file)) {
                    stream.write(data);
                } catch (IOException e) {
                    System.err.println("error reading file_data");
                    file = null;
                    break;
                }
                break;
            }
        }

        return file;

    }

}
