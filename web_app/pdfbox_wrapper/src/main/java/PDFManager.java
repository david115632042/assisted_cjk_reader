
import java.io.File;
import java.io.IOException;
import java.util.Base64;

import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.io.RandomAccessFile;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.bson.Document;


public class PDFManager {

    private PDFParser parser;
    private PDFTextStripper pdfStripper;
    private PDDocument pdDoc ;
    private COSDocument cosDoc ;

    private String Text ;

    public PDFManager() {

    }

    public Document toText(File file, int startPage, int endPage) throws IOException {

        parser = new PDFParser(new RandomAccessFile(file,"r")); // update for PDFBox V 2.0

        parser.parse();
        cosDoc = parser.getDocument();
        pdfStripper = new PDFTextStripper();
        pdDoc = new PDDocument(cosDoc);
        int num_of_pages = pdDoc.getNumberOfPages();
        pdfStripper.setStartPage(startPage);
        pdfStripper.setEndPage(endPage);

        Text = pdfStripper.getText(pdDoc);

        pdDoc.close();

        byte text_bytes[] = Text.getBytes("UTF-8");
        String base64_str = Base64.getEncoder().encodeToString(text_bytes);

        Document doc = new Document();
        doc.put("num_of_pages", num_of_pages);
        doc.put("text", base64_str);
        return doc;

    }

    public Document getPages(File file) throws IOException {

        parser = new PDFParser(new RandomAccessFile(file,"r")); // update for PDFBox V 2.0

        parser.parse();
        cosDoc = parser.getDocument();
        pdDoc = new PDDocument(cosDoc);

        Document return_doc = new Document();
        return_doc.put("num_of_pages", pdDoc.getNumberOfPages());

        pdDoc.close();

        return return_doc;
    }

}
