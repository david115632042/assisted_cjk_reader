
import { Mecab } from "Api";

export default function($rootScope, $stateParams, server_data) {
    var vm = this;

    vm.output_details_controller = {};

    vm.query_mecab = query_mecab;
    vm.load_details = load_details;
    vm.set_word_class = set_word_class;

    init();

    function init () {
        query_mecab($stateParams.input_text);
    }

    function query_mecab (text_in): void {
        if (text_in.length === 0) { return; }
        vm.loading = true;

        server_data.query_mecab(text_in, "pretty").then(function success (response) {
            vm.words_array = response.data;
            vm.loading = false;
        }, function error (response) {
            vm.loading = false;
        });
    }

    function load_details (token: Mecab.MecabToken): void {
        var word = (token.root_form && token.root_form !== "*") ? token.root_form : token.surface_form;
        vm.output_details_controller.load_details(word);
    }

    function set_word_class (word: Mecab.PosMember): String {
        if (word === "noun") {
            return "output-word-N";
        } else if (word === "particle") {
            return "output-word-Pa";
        } /*else if (word === "symbol") {
            return "output-word-Sy";
        }*/ else if (word === "adjective") {
            return "output-word-Adj";
        } else if (word === "verb") {
            return "output-word-V";
        } else if (word === "adverb") {
            return "output-word-Adv";
        } else if (word === "auxiliary verb") {
            return "output-word-AuxV";
        }
    }

};