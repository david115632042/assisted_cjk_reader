
import org.bson.Document;

import java.io.*;
import java.net.Socket;


public class Client_handler extends Thread {

    private Socket client_socket;
    private Upload_db db;

    public Client_handler(Socket _client_socket_, Upload_db _db_) {
        this.client_socket = _client_socket_;
        this.db = _db_;
    }


    private String handle_command(String [] args) {
        Document return_status = new Document();
        Args my_args = Parse_args.parse(args);

        File file = db.get_file(my_args.user_id, my_args.file_id);

        if (my_args.command.equals("extract")) {

            PDFManager pdfManager = new PDFManager();

            Document output = null;
            try {
                output = pdfManager.toText(file, my_args.start_page, my_args.end_page);
            } catch (IOException e) {
                System.err.println("error with pdfManager: toText");
                System.exit(1);
            } finally {
                if (output == null) {
                    System.err.println("toText output was null");
                    System.exit(1);
                }
            }


            return_status.put("response", output);

            //System.out.println(return_status.toJson());
            return return_status.toJson();

        } else if (my_args.command.equals("pages")) {

            PDFManager pdfManager = new PDFManager();

            Document output = null;
            try {
                output = pdfManager.getPages(file);
            } catch (IOException e) {
                System.err.println("error with pdfManager: getPages");
                System.exit(1);
            } finally {
                if (output == null) {
                    System.err.println("output was null");
                    System.exit(1);
                }
            }

            return_status.put("response", output);
            //System.out.println(return_status.toJson());
            return return_status.toJson();

        } else {
            return null;
        }
    }


    @Override
    public void run() {

        BufferedReader in = null;
        PrintWriter out = null;

        try {
            in = new BufferedReader(
                    new InputStreamReader(client_socket.getInputStream()));
            out = new PrintWriter(client_socket.getOutputStream(), true);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        } finally {
            if (in == null || out == null) {
                System.out.println("client io could not be created");
                System.exit(1);
            }
        }


        String input = null;
        try {
            input = in.readLine();
            if (input == null) {
                this.client_socket.close();
                return;
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        } finally {
            if (input == null) {
                System.out.println("error reading input");
            }
        }

        String [] args = null;
        try {
            args = input.split(" ");
        } catch (NullPointerException e) {
            System.err.println("error parsing command");
        }


        String response = handle_command(args);
        if (response != null) {
            out.println(response);
        } else {
            Document return_status = new Document();
            return_status.put("error", "error parsing input");
            out.println(return_status.toJson());
        }

        out.flush();

        try {
            client_socket.close();
        } catch (IOException e) {
            System.err.println("error closing client socket");
        }


    }

}
