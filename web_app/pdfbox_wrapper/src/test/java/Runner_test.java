
import static org.junit.Assert.assertEquals;

import org.bson.Document;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.concurrent.*;

public class Runner_test {

    private static String CORRECT_PDF_CONTENT = "TCPDF Example 008\n" +
            "by Nicola Asuni - Tecnick.com\n" +
            "www.tcpdf.org\n" +
            "Sentences that contain all letters commonly used in a language\n" +
            "--------------------------------------------------------------\n" +
            "This file is UTF-8 encoded.\n" +
            "Czech (cz)\n" +
            "---------\n" +
            "  Příšerně žluťoučký kůň úpěl ďábelské ódy.\n" +
            "  Hleď, toť přízračný kůň v mátožné póze šíleně úpí.\n" +
            "  Zvlášť zákeřný učeň s ďolíčky běží podél zóny úlů.\n" +
            "  Loď čeří kýlem tůň obzvlášť v Grónské úžině.\n" +
            "  Ó, náhlý déšť již zvířil prach a čilá laň teď běží s houfcem gazel k úkrytům.\n" +
            "Danish (da)\n" +
            "---------\n" +
            "  Quizdeltagerne spiste jordbær med fløde, mens cirkusklovnen\n" +
            "  Wolther spillede på xylofon.\n" +
            "  (= Quiz contestants were eating strawbery with cream while Wolther\n" +
            "  the circus clown played on xylophone.)\n" +
            "German (de)\n" +
            "-----------\n" +
            "  Falsches Üben von Xylophonmusik quält jeden größeren Zwerg\n" +
            "  (= Wrongful practicing of xylophone music tortures every larger dwarf)\n" +
            "  Zwölf Boxkämpfer jagten Eva quer über den Sylter Deich\n" +
            "  (= Twelve boxing fighters hunted Eva across the dike of Sylt)\n" +
            "  Heizölrückstoßabdämpfung\n" +
            "  (= fuel oil recoil absorber)\n" +
            "  (jqvwxy missing, but all non-ASCII letters in one word)\n" +
            "English (en)\n" +
            "------------\n" +
            "  The quick brown fox jumps over the lazy dog\n" +
            "Spanish (es)\n" +
            "------------\n" +
            "  El pingüino Wenceslao hizo kilómetros bajo exhaustiva lluvia y\n" +
            "  frío, añoraba a su querido cachorro.\n" +
            "  (Contains every letter and every accent, but not every combination\n" +
            "                               page 1 / 3\n";

    private static int program_port = 5001;
    volatile boolean POLLING_MEMORY = true;


    @Test
    public void test() {

        long initial_memory_usage = Runtime.getRuntime().totalMemory() -
                Runtime.getRuntime().freeMemory();
        System.out.println("initial memory usage (kb): " + initial_memory_usage / 1024);

        new Thread(() -> {
            Runtime runtime = Runtime.getRuntime();
            while (POLLING_MEMORY) {
                long memory_usage = runtime.totalMemory() - runtime.freeMemory();
                System.out.println("memory usage (kb): " + memory_usage / 1024);
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        Mock_upload_db db = new Mock_upload_db();

        Runner runner = new Runner(db, program_port);
        new Async_program_runner(runner).start();

        int MAX_CONCURRRENT_OPERATIONS = Runtime.getRuntime().availableProcessors();
        int NUM_OF_TASKS = 1;

        ExecutorService executor = Executors.newFixedThreadPool(MAX_CONCURRRENT_OPERATIONS);

        List<Future<String>> vals = new ArrayList<>();

        for (int i = 0; i < NUM_OF_TASKS; i++) {
            vals.add(executor.submit(client_task));
        }

        try {
            for (Future<String> future: vals) {
                String return_val = future.get();
                assertEquals(return_val, CORRECT_PDF_CONTENT);
            }

            POLLING_MEMORY = false;

        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

    }

    private Callable<String> client_task = () -> {

        try (Socket client = new Socket("127.0.0.1", program_port)) {

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(client.getInputStream()));
            PrintWriter out = new PrintWriter(client.getOutputStream(), true);

            String extract_command = "extract --user_id 578c7a96feec9d10009fc9a4 --file_id test.pdf --pages 0 1";

            out.println(extract_command);
            out.flush();

            String runner_response = in.readLine();

            // parse JSON output
            Document response_doc = Document.parse(runner_response);
            byte bytes[] = Base64.getDecoder().decode(((Document)response_doc.get("response")).getString("text"));
            String input_string = new String(bytes, "UTF-8");

            return input_string;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    };

    private class Async_program_runner extends Thread {

        private Runner runner;

        public Async_program_runner(Runner _runner_) {
            this.runner = _runner_;
        }

        @Override
        public void run() {
            this.runner.run();
        }
    }

}
