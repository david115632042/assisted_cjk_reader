import * as express from "express";
var router = express.Router();

import * as api_routing from "./api";
import * as user_routing from "./user";
import * as view_routing from "./view";

export function get_routing (passport) {

    router.use('/api', api_routing.get_routing());
    router.use('/user', user_routing.get_routing(passport));
    router.use('/view', view_routing.get_routing());

    return router;

}