
var assert = require('assert');

window.jQuery = window.$ = require('jquery');
window.toastr = require('toastr/toastr.js');

var angular = require('angular');
require('angular-mocks');

var app = angular.module('testApp', []);

app.service('server_data', require('../../src/js/services/server_data_service.js'));
app.directive('fileSelector', require('../../src/analyzer/components/file_selector/file_selector.js'));

var filename = "filename.txt";
var filetype = 'txt';
var filesize = 'filesize';
function select_file(file_selector_elem) {

  var scope = file_selector_elem.isolateScope();
  scope.file = new File([], filename);
  scope.file_input = {
    name: filename,
    type: filetype,
    size: filesize
  }
  scope.$digest();

}

function get_file_badge (file_selector) {
  return file_selector.find('.file-selector-browse-badge-container');
}

function get_button_container (file_selector) {
  return file_selector.find('.file-selector-submission-btn-container');
}

function file_badge_and_Buttons_are_visible (file_selector) {
  return (get_file_badge(file_selector).html().includes('id="file-selected-badge"') 
      && get_button_container(file_selector).html().includes('id="file-remove-btn"'));
}

function file_badge_and_Buttons_are_invisible (file_selector) {
  return (!get_file_badge(file_selector).html().includes('id="file-selected-badge"') 
      && !get_button_container(file_selector).html().includes('id="file-remove-btn"'));
}

describe('testing file_selector directive', function () {
  var $controller;
  var $compile;
  var $rootScope;
  var $httpBackend;

  beforeEach(angular.mock.module('testApp'));

  // Store references to $rootScope and $compile
  // so they are available to all tests in this describe block
  beforeEach(inject(function (_$controller_, _$compile_, _$rootScope_, $injector) {
    // The injector unwraps the underscores (_) from around the parameter names when matching
    $controller = _$controller_;
    $compile = _$compile_;
    $rootScope = _$rootScope_;
    $httpBackend = $injector.get('$httpBackend');
  }));

  function get_fileSelector() {
    var element = $compile("<file-selector></file-selector>")($rootScope);
    $rootScope.$digest();
    return element;
  }

  it('replaces the element with the appropriate content', function () {

    var element = get_fileSelector();
    assert(element.find('#browse-file-button').html() != null);

  });

  it('should reveal the file_badge and selector buttons upon file selection', function () {

    var element = get_fileSelector();

    assert(file_badge_and_Buttons_are_invisible(element));

    select_file(element);

    assert(file_badge_and_Buttons_are_visible(element));

  });

  it('should display the selected filename', function () {

    var element = get_fileSelector();

    select_file(element);

    assert(get_file_badge(element).html().includes(filename));

  });

  describe('file action buttons', function () {

    it('should perform file upload', function (done) {

      var element = get_fileSelector();

      select_file(element);

      $httpBackend.expectPOST('/user/file/upload').respond(200, '');
      element.find('#file-upload-btn').triggerHandler('click');

      setTimeout(function () {
        $httpBackend.flush();
        console.log('TEST DONE');
        done();
      });

    });

    it('should clear file input badge after successful submission', function (done) {

      var element = get_fileSelector();

      select_file(element);

      $httpBackend.expectPOST('/user/file/upload').respond(200, '');
      element.find('#file-upload-btn').triggerHandler('click');

      setTimeout(function () {
        $httpBackend.flush();
        console.log('TEST DONE');

        assert(file_badge_and_Buttons_are_invisible(element));

        done();
      });

    });

    it('should not clear file input badge after failed submission', function (done) {

      var element = get_fileSelector();

      select_file(element);

      $httpBackend.expectPOST('/user/file/upload').respond(409, '');
      element.find('#file-upload-btn').triggerHandler('click');

      setTimeout(function () {
        $httpBackend.flush();
        console.log('TEST DONE');

        assert(file_badge_and_Buttons_are_visible(element));

        done();
      });

    });

  });

});
