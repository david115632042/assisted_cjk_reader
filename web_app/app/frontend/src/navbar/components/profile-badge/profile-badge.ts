
import { ServerData, DisplayState } from "Client";

export default function(server_data: ServerData, display_state: DisplayState) {

    return {
        restrict: "E",
        scope: true,
        template: require("./profile-badge.html"),
        link: function( $scope, elem, attrs ) {
            
            $scope.logged_in = display_state.logged_in;
            $scope.pressed_logout_btn = logout;

            $scope.$watch(() => { return display_state.logged_in; }, handle_logged_in_updated);

            function logout() {
                server_data.logout().then(function successCallback(response) {
                    toastr.success("logged out");
                    display_state.alert_logged_out();
                }, function errorCallback(response) {
                    if (response.status === 401) {
                        toastr.success("logged out");
                        display_state.alert_logged_out();
                    } else {
                        toastr.error("error logging out");
                    }
                    
                });
            }

            function handle_logged_in_updated(new_logged_in_status) {
                $scope.logged_in = new_logged_in_status;
            }

        }
    };
};