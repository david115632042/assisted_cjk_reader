var gulp = require("gulp");
var gutil = require("gulp-util");
var ejs = require("gulp-ejs");
var sass = require("gulp-sass");
var connect = require('gulp-connect');
var concat = require("gulp-concat");
var webpack = require("webpack");
var del = require('del');
var mocha = require('gulp-mocha');
var async = require('async');

var tsc = require("gulp-typescript");
var babel = require("gulp-babel");

var webpack_backend_config = require('./server/webpack.config.js');
var webpack_dev_config = require('./frontend/webpack.dev.config.js');
var webpack_production_config = require('./frontend/webpack.production.config.js');

var filesToMove = [
    "./frontend/src/index.html",
    "./frontend/src/templates/**",
    "./frontend/src/fonts/**"
];


gulp.task('clean', function () {
    return del(['./frontend/public/**',
    '!./frontend/public']);
});

gulp.task('clean_server', function () {
    return del(['./server/dist/**',
    '!./server/dist']);
});

gulp.task('clean_test_compile', function () {
    del(['./test/test_compile/**',
        '!./test/test_compile/']);
});


gulp.task('build-styles', function() {
    gulp.src('./frontend/src/sass/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./frontend/public/css/'));
});


function build_frontend (config, done) {
    webpack(config, function (err, stats) {
        gutil.log("[webpack]", stats.toString({
            // output options
        }));
        if (err) throw new gutil.PluginError("webpack", err);
        gutil.log("[webpack]", stats.toString({
            // output options
        }));
        console.log("webpack finished");

        gulp.src(filesToMove, {base: './frontend/src'})
            .pipe(gulp.dest('./frontend/public'));

        console.log('files finished moving');

        done();

    });
};


function build_backend_old () {

    var tsProject = tsc.createProject('./server/tsconfig.json');
    gulp.src([
            "server/**/**.ts"
        ])
        .pipe(tsProject())
        .pipe(babel())
        .pipe(gulp.dest("server/dist"));

    gulp.src("./server/views/**", {base: "./server"}).pipe(gulp.dest("./server/dist"));

}


function build_backend (done) {

    webpack(webpack_backend_config, function (err, stats) {
        gutil.log("[webpack]", stats.toString({
            // output options
        }));
        if (err) throw new gutil.PluginError("webpack", err);
        gutil.log("[webpack]", stats.toString({
            // output options
        }));
        console.log("webpack finished");

        gulp.src("./server/views/**", {base: "./server"}).pipe(gulp.dest("./server/dist"));

        done();

    });

}


function test () {

    return gulp.src(['server/test/**/*.js'], { read: false })
        .pipe(mocha({
            reporter: 'spec',
            timeout: 3000,
            globals: {
            }
        }))
        .once('error', function () {
            process.exit(1);
        })
        .once('end', function () {
            process.exit();
        });

}


gulp.task('build_front_dev', ['clean'], function (done) {
    build_frontend(webpack_dev_config, done);
});


gulp.task('build_front_production', ['clean'], function (done) {
    build_frontend(webpack_production_config, done);
});

gulp.task('build_back', ['clean_server'], function (done) {
    build_backend(done);
});

gulp.task('build_all_dev', ['build_front_dev', 'build_back'], function () {

});

gulp.task('watch_build_front', function() {
    var watch_list = ['./frontend/src/**/*'];
    gulp.watch(watch_list, ['build_front_dev']);
});


gulp.task('dev-server', ['build_dev'], function () {
    connect.server({
        root: "frontend/public",
        livereload: true,
        port: 4000
    });

    var watch_list = ['./frontend/src/**'];
    gulp.watch(watch_list, ['build_dev']);

});


gulp.task('ejs_test_compile', function() {
    gulp.src("./server/views/*.ejs")
        .pipe(ejs({
            message: null,                          // for index.ejs
            words_array: [],                        // for analyzer_segmented.ejs
            entry_list: [],                         // for popover.ejs
            error: {status: "", stack: ""}          // for error.ejs
        })).on('error', gutil.log);
});


var typedoc = require("gulp-typedoc");
gulp.task("doc_frontend", function() {
    return gulp
        .src(["my_modules/**/*.ts", "frontend/my_modules/**/*.ts", "frontend/typings/**/*.ts", "./frontend/src/**/*.ts"])
        .pipe(typedoc({ 
            module: "commonjs", 
            target: "es6",
            includeDeclarations: false,
            experimentalDecorators: true,
            out: "./frontend/docs", 
            name: "JPReader", 
            ignoreCompilerErrors: false,
            excludeExternals:true,
            version: true,
        }));
});