import * as express from "express";
var router = express.Router();

var async = require('async');
import require_auth from "../../require_authentication";

function get_newline_positions(text, position_list = [], start_search_position = 0) {
    var new_found_index = text.indexOf('\n', start_search_position);
    if (new_found_index == -1) return position_list;
    else {
        position_list.push(new_found_index);
        return get_newline_positions(text, position_list, new_found_index + 1);
    }
}

export function get_routing() {

    router.get('/', require_auth, function (req, res, next) {

        console.log('DOWNLOAD ENDPOINT HIT');

        var file_id = req.query.file_id;
        var page = Number(req.query.page);

        req.pdfbox_service.extract_pdf_text(req.user._id, file_id, page, page + 1, function (response) {

            console.log('DOWNLOAD RESPONSE GOES HERE');
            if (response == null) {
                res.status(404);
                res.json({ error: "couldn't process file" });
            }
            else {
                console.log('DOWNLOAD RESPONSE: ' + JSON.stringify(response));
                console.log(response.text);

                var text = response.text;

                // get positions of all newline characters
                var newline_positions = get_newline_positions(text);

                // strip them before sending them through mecab
                text = text.replace(/[\n\ ]/g, '');
                var sentences = text.split(/。|\./g);

                var parsed_sentences = [];

                async.forEachOf(sentences, function (sentence, index, callback) {

                    console.log("sentence: " + sentence);
                    req.mecab_service.parse(sentence, function (err, items) {
                        if (err) {
                            console.log("error parsing input: ERROR: " + err + "SENTENCE: " + sentence);
                        } else {
                            console.log("DOWNLOAD: MECAB_INPUT: " + sentence);
                            console.log("DOWNLOAD: MECAB_OUTPUT: " + JSON.stringify(items, null, 2));
                            parsed_sentences[index] = req.mecab_service.format_mecab(items);
                        }
                        callback();

                    });

                }, function (err) {
                    res.json({ pages: response.num_of_pages, text: parsed_sentences });
                });

            }

        });

    });

    return router;

}