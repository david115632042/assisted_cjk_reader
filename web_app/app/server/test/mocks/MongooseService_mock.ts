/*
import * as mongoose from "mongoose";
mongoose.Promise = require("q").Promise;

import {fileSchema, FileDoc} from "../../db/schemas/fileSchema";
import {uploadsSchema, UploadsDoc} from "../../db/schemas/uploadsSchema";
import {userSchema, UserDoc} from "../../db/schemas/userSchema";
import {jmDictSchema, JmDictDoc} from "../../db/schemas/jmDictSchema";


export default class MongooseService implements MongooseService {

    private connection: mongoose.Connection;
    private file_model: mongoose.Model<mongoose.Document>;
    private uploads_model: mongoose.Model<mongoose.Document>;
    private user_model: mongoose.Model<mongoose.Document>;

    constructor () {

    }

    public connect (url, done: (err: Error) => void) {
        this.connection = mongoose.createConnection(url, (err: Error) => {
            if (err) console.log('MONGOOSE CONNECT ERROR: ' + err);
            this.connection.model("File", fileSchema);
            this.connection.model("Uploads", uploadsSchema);
            this.connection.model("User", userSchema);
            this.file_model = this.connection.model("File");
            this.uploads_model = this.connection.model("Uploads");
            this.user_model = this.connection.model("User");
            done(err);
        });
    }


    public close (done: () => void) {
        this.connection.close(() => {
            done();
        })
    }
    

    public save_upload (user_id, file: dbType.File, callback: (err: String, updated_uploads: any) => void) {

        var new_file_doc = <FileDoc>(new this.file_model(file));

        this.uploads_model.findOne({ 'user_id': user_id }, (err, user_uploads: UploadsDoc) => {

            if (err) {
                console.error(err);
                callback(err, null);
                return;
            }

            if (user_uploads) {
                user_uploads.file_list.push(new_file_doc);
                user_uploads.save(function (err, updated_uploads) {
                    callback(err, updated_uploads);
                    return;
                });
            } else {
                var new_user_upload = new this.uploads_model({ user_id: user_id, file_list: [new_file_doc] });
                new_user_upload.save(function (err, updated_uploads) {
                    callback(err, updated_uploads);
                    return;
                });
            }

        });

    }


    public delete_upload(user_id, file_id, callback: (err: String, user_uploads: any) => void) {
        this.uploads_model.findOne({ 'user_id': user_id }, (err, user_uploads: UploadsDoc) => {

            if (err) {
                console.error(err);
                callback(err, null);
                return;
            }

            if (user_uploads) {
                console.log("SEARCHING FOR FILE_ID: " + file_id);
                user_uploads.file_list.forEach(function (element: FileDoc) {
                    console.log("FILE_ID: " + element._id);
                }, this);

                user_uploads.file_list = user_uploads.file_list.filter(function (file: FileDoc) {
                    return (file._id != file_id);
                }, this);

                console.log("NEW LIST");

                user_uploads.file_list.forEach(function (element: FileDoc) {
                    console.log("FILE_ID: " + element._id);
                }, this);

                user_uploads.save(function (err, updated_uploads) {
                    callback(err, updated_uploads);
                    return;
                });
            } else {
                callback("error", null);
            }

        });

    };

    public get_user_file_list (user_id, callback: (err: String, user_uploads: any) => void) {
        this.connection.model("Uploads").findOne({ 'user_id': user_id }, '-file_list.filedata', (err, user_uploads) => {

            if (err) {
                callback(err, null);
                return;
            }

            callback(null, user_uploads);

        });
    }

    public create_user(username, password, callback: (err, new_user) => void): void {
        // create the user
        var newUser = <UserDoc>(new this.user_model);

        // set the user's local credentials
        newUser.local.username = username;
        newUser.local.password = newUser.generateHash(password);

        // save the user
        newUser.save(function (err) {
            callback(err, newUser);
        });

    }

    public get_user_by_id (id, callback: (err, user) => void): void {
        this.connection.model("User").findById(id, function(err, user) {
            callback(err, user);
        });
    }

    public get_user_by_username (username, callback: (err, user) => void): void {
        this.connection.model("User").findOne({ 'local.username' :  username }, function(err, user) {
            callback(err, user);
        });
    }

}
*/